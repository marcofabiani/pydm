#!usr/bin/env/ python
# pyDM : python pdm player and performance control
# Copyright (c) Marco Fabiani 2009
#
# oscDaemon.py - Manage OSC incoming and outgoing messages

from PyQt4 import QtCore
from PyQt4 import QtGui
import osc
#from numpy import *
import array,threading,Queue
from time import sleep,time,localtime,strftime,ctime
from math import sqrt
import os

__FACTOR__ = 36864/4
__LOGACC__ = False
__MAXBOUNCEPERSEC__ = 10
__BOUNCEBUFFERSIZE__ = 40
__LOGDIR__ = os.path.expanduser('~')+'/pyDMlogs/'
def autocorr(vect,n_lags = None):
	""" Computes autocorrelation vector using the FFT method for speed (see xcorr.m in matlab): non optimal (can't select number of lags...)
		"""
	# Compute the fft (length of vect must be a power of two for better performance)
	n = 2*len(vect)
	X = fft.rfft(vect,n)
	c = array.array('f')
	# Multiply the fft
	for item in X:
		c.append(pow(abs(item),2))
	
	# inverse transform and take the abs
	c = fft.irfft(c,n)
	
	# positive lags
	if not n_lags:
		n_lags = n/2
	c = c[:n_lags]
	return c
	
def modulus(vect):
	""" Sum of the squares of the x,y,z coordinates from the accelerometer
	"""
	return pow(vect[0],2)+pow(vect[1],2)+pow(vect[2],2)

class OSCDaemon(QtCore.QThread):
	"""Manage OSC messages: use a loop to keep listening to messages!
	"""
#	def __init__(self,bufferSize = 100, lags = None, parent = None):
	def __init__(self,parent = None):
	
		super(OSCDaemon,self).__init__(parent)
		self.xRange = [-1.0,1.0]
		self.yRange = [-1.0,1.0]
		self.ipOut = '127.0.0.1'
		self.tcpOut = 9000
		self.accIn = False
		self.xyAddress = '/pyDM/xy'
		self.accAddress = '/pyDM/acc'
		self.accRawAddress = '/pyDM/accraw'
		self.__logacc = __LOGACC__
		self.__w = [0.9,0.1]
		self.__r = [0.0,0.0]
		self.__deltaA = [0.5,0.5]
		self.__logHandle = None
		self.__currentFile = 'Berio';

		#self.__results = Queue.Queue(3)
		
#	def updateBuffSize(self,buffSize):
#		"""Updates acc. data buffer size
#		"""
#		if self.__listening:
#			wasListening = True
#			self.stopOSC()
#		self.__buffer0 = Queue.Queue(buffSize)
#		self.__buffer1 = Queue.Queue(buffSize)
#		self.__activeBuffer = self.__buffer0
#		self.__fullBuffer = self.__buffer1
#		if wasListening:
#			self.startOsc(self.tcpIn,self.ipOut,self.tcpOut,self.xyAddress,self.xRange,self.yRange)
	
	#def startOSC(self,tcpPortIn,ipAddrOut,tcpPortOut,xyAddress,xRange,yRange,bufferSize):	
	def startOSC(self,tcpPortIn,ipAddrOut,tcpPortOut,xyAddress,xRange,yRange,bufferSize,tempo):
		"""
		Start listening for incoming OSC messages (start the loop)
		"""
		# Accelerometer buffers
		# Use two buffers: when one is full, analyze it and switch to the other one
		#print bufferSize
		#print tcpPortOut
		bufferSize = int(round(bufferSize/2))
		self.__bufferSize = bufferSize
		self.__buffer0 = Queue.Queue(bufferSize)
		self.__buffer1 = Queue.Queue(bufferSize)
		self.__oldValues = array.array('f') # This is used to do the half buffer overlap!
		
		for i in range(0,bufferSize):
			self.__oldValues.append(0.0)
			
		self.__activeBuffer = self.__buffer0
		self.__fullBuffer = self.__buffer1
		self.__listening = False
		self.__lags = bufferSize
		
		self.__listening = True
		osc.init()
		# Listen to incoming messages
		osc.listen('',tcpPortIn)
		
		# Set out IP and port
		self.tcpIn = tcpPortIn
		self.ipOut = ipAddrOut
		self.tcpOut = tcpPortOut
		#self.corners = corners
		self.xRange = xRange
		self.yRange = yRange
		self.__playrecState = False
		
		# Set input addresses
		self.xyAddress = xyAddress
		
		# Start listening to control messages
		osc.bind(self.recvControl,'/pyDM/control')
		#osc.bind(self.recvXY,self.xyAddress)
		#print 'started'
		
		#######################
		# Create a new log file (name = {date}.log)
		if self.__logacc:
			self.__offset = 1000*time() #ms
			dateStr = localtime()				
			fileName = __LOGDIR__ + strftime('%Y%m%d-%H%M%S',dateStr) +'_'+ self.__currentFile +'.log'
			
			if not os.path.isdir(__LOGDIR__):
				os.mkdir(__LOGDIR__)
			try:
				self.__logHandle = open(fileName, 'w')
				self.__logHandle.write(strftime('%a, %d %b %Y %H:%M:%S',dateStr)+'\n'+'pyDM Agora accelerometers log\n\n')
			except:
				print 'Error: couldnt open log file'
			
		# Start listening to data
		self.startAcc(False)
		
		# For the raw acc analysis
		self.__onState = False
		self.__prevOnset = 0.0
		self.__onThr = -0.7
		self.__tempo = 60.0/float(tempo)
		self.__currentNotesPerSec = 1.0
		self.__tempAcc = 0
		self.__timeOffset = time()
		
		# For the bounce analysis
		self.__prevBounce = [time(),1]
		self.__prevBouncePerSec = [1.0,1.0,1.0,1.0]
		self.__prevValence = 0.0
		#self.__wallCount = [(__BOUNCEBUFFERSIZE__/2) , (__BOUNCEBUFFERSIZE__/2)];
		#self.__wallCount = [(__BOUNCEBUFFERSIZE__/4) , (__BOUNCEBUFFERSIZE__/4) , (__BOUNCEBUFFERSIZE__/4) , (__BOUNCEBUFFERSIZE__/4)]
			
	def startAcc(self,value = True):
		# Start listening to messages from XY or Acc
		self.accIn = value
		if self.accIn: # Start ACC
			try:
				osc.bind(None,self.xyAddress)
			except:
				pass
			try:
				#osc.bind(self.recvAcc,self.accAddress)
				#osc.bind(self.recvAccBeat,self.accAddress)
				#self.__prevOnset = time()
				#osc.bind(self.recvBounce,self.accAddress)
				osc.bind(self.recvRaw,self.accRawAddress)
				#print self.accAddress
			except:
				print "OSC was not started yet"
			try:
				if self.__logacc:
					self.__logHandle.write('Wall bounces data:\n')
			except:
				pass
			#	print "couldnt open log file to write"
		else: # Start XY
			try:
				#osc.bind(None,self.accAddress)
				osc.bind(None,self.accRawAddress)
			except:
				pass
			try:
				osc.bind(self.recvXY,self.xyAddress)
				#print self.xyAddress
			except:
				print "OSC was not started yet"
			try:
				if self.__logacc:
					self.__logHandle.write('Activity Valence data:\n')
			except:
				pass
			#	print "couldnt close log file"

	def recvRaw(self,*msg):
		"""
		Log raw accelerometer data
		"""
		#print msg[0]
		
		x = msg[0][2] # used to control if the phone is up or down
		y = msg[0][3] # used to compute the 
		z = msg[0][4]
		
		
		if self.__logacc:
			self.__logHandle.write('raw, ' + str(x) + ',' + str(y) + ',' + str(z) +'\n')
		
		accPower = pow(y,2) + pow(z,2) + pow(x,2)
		
		#self.__activeBuffer.put(power)
		self.__activeBuffer.put([accPower , x])
		if self.__activeBuffer.full():
			#print str(msg[0][2])+','+str(msg[0][3])+','+str(msg[0][4])+'\n'
			tmp = self.__activeBuffer
			self.__activeBuffer = self.__fullBuffer
			self.__fullBuffer = tmp
			#print 'full'
			threading.Thread(target = self.__powerAnalysis).start()
		# Check if it is up or down:
		"""valence = -x/0.5
				
		# Check if out of bounds
		#if activity > 0.999:
		#	activity = 0.999
		#elif activity < -0.999:
		#	activity = -0.999
		if valence > 0.999:
			valence = 0.999
		elif valence < -0.999:
			valence = -0.999
			
		#activity = activity * 0.8
		#valence = valence * 0.8
		
		# Emit signals
		if not self.__playrecState:
			#self.emit(QtCore.SIGNAL("OSCactivity(double)"),float(activity))
			self.emit(QtCore.SIGNAL("OSCvalence(double)"),float(valence))"""
			
	def __powerAnalysis(self):
		"""
		Uses autocorrelation to analyze the gestures, and outputs periodicity and amplitude
		"""
		#print 'hello'
		power_avg = 0.0
		x_avg = 0.0
		accum = 0
		#y = array.array('f')
		#z = array.array('f')
		# Read from the buffer and compute the squared modulus of the acceleration
		while not self.__fullBuffer.empty():
			data = self.__fullBuffer.get()
			power_avg += data[0]
			x_avg += data[1]
			accum += 1
			#z.append(data[1])
		power_avg = power_avg/accum - 0.25
		x_avg = x_avg/accum
		valence = -x_avg/0.45
		#print power_avg
		activity = 2*power_avg/0.70 - 1
		# Check if out of bounds
		if activity > 0.999:
			activity = 0.999
		elif activity < -0.999:
			activity = -0.999
			
		if valence > 0.999:
			valence = 0.999
		elif valence < -0.999:
			valence = -0.999
			
		#print activity,valence
			
		self.emit(QtCore.SIGNAL("OSCactivity(double)"),float(activity))
		self.emit(QtCore.SIGNAL("OSCvalence(double)"),float(valence))
		
	
	def recvXY(self,*msg):
		"""
		Process OSC messages for X and Y
		"""
		#print msg[0]
		# Convert OSC coordinates to the GUI coordinates
		#x = -1 + 2*(msg[0][2] - self.corners[0])/(self.corners[1]-self.corners[0])
		#y = -1 + 2*(msg[0][3] - self.corners[2])/(self.corners[3]-self.corners[2])
		#print x,y
		x = -1 + 2*(msg[0][2] - self.xRange[0])/(self.xRange[1]-self.xRange[0])
		y = -1 + 2*(msg[0][3] - self.yRange[0])/(self.yRange[1]-self.yRange[0])	
		
		if x>0.999:
			x = 0.999
		elif x< -0.999:
			x = -0.999
			
		if y> 0.999:
			y = 0.999
		elif y < -0.999:
			y = -0.999
		
		
		#print msg[0][2],msg[0][3]
		if not self.__playrecState:
			self.emit(QtCore.SIGNAL("OSCx(double)"),x)
			self.emit(QtCore.SIGNAL("OSCy(double)"),y)
		if self.__logacc:
			self.__logHandle.write(str(1000*time()-self.__offset)+' , '+str(x)+' , '+str(y)+'\n')
	
	def stopOSC(self):
		"""
		Stop listening for incoming OSC messages
		"""
		self.__listening = False
		if self.__logacc:
			try:
				self.__logHandle.close()
			except:
				pass
		try:
			osc.dontListen()
			self.sendOSC = False
		except:
			pass
			
	def recvControl(self,*msg):
		"""
		Receive control messages and parse them, send SIGNALS
		"""
		command = msg[0][2]
		if command == 'play':
			self.__playrecState = False
			self.__logHandle.write(str(1000*time()-self.__offset)+' , play\n')
			self.emit(QtCore.SIGNAL("OSCPlay(bool)"),msg[0][3])
		elif command == 'stop':
			self.__playrecState = False
			self.__logHandle.write(str(1000*time()-self.__offset)+' , stop\n')
			self.emit(QtCore.SIGNAL("OSCStop()"))
		elif command == 'rec':
			self.__playrecState = False
			self.__logHandle.write(str(1000*time()-self.__offset)+' , rec\n')
			self.emit(QtCore.SIGNAL("OSCRec(bool)"),msg[0][3])
		elif command == 'playrec':
			self.__playrecState = True
			self.__logHandle.write(str(1000*time()-self.__offset)+' , playrec\n')
			self.emit(QtCore.SIGNAL("OSCPlayRec(bool)"),msg[0][3])
			self.emit(QtCore.SIGNAL("OSCAcc(bool)"),False)
		elif command == 'acc':
			self.__playrecState = False
			self.__logHandle.write(str(1000*time()-self.__offset)+' , acc\n')
			self.emit(QtCore.SIGNAL("OSCAcc(bool)"),msg[0][3])
		elif command == 'repeat':
			self.__logHandle.write(str(1000*time()-self.__offset)+' , repeat\n')
			self.emit(QtCore.SIGNAL("OSCRepeat(bool)"),msg[0][3])
		elif command == 'file':
			self.__logHandle.write(str(1000*time()-self.__offset)+' , file change\n')
			if msg[0][3] == 1:
				self.__currentFile = 'Berio'
			elif msg[0][3] == 2:
				self.__currentFile = 'Joplin'
			elif msg[0][3] == 3:
				self.__currentFile = 'Mozart'
			else:
				pass
			self.emit(QtCore.SIGNAL("OSCfile(int)"),msg[0][3])
		else: pass

	def recvBounce(self,*msg):
		"""
		Receive bounce on the wall (wall number 1,2,3,4)
		"""
		wall = int(msg[0][2])
		#print wall
		bounce = time()
		if self.__logacc:
			self.__logHandle.write(str(1000*time()-self.__offset)+' , '+str(wall)+'\n')
		#print self.__wallCount
		#print self.__prevBounce
		
		# Bounce per second, include some smoothing
		currBouncePerSec = 1/((bounce-self.__prevBounce[0]))
		
		if not (currBouncePerSec > __MAXBOUNCEPERSEC__ and wall == self.__prevBounce[1]): # Filter out microbounces
		
			bouncePerSec = (self.__prevBouncePerSec[3] * 2.0 + self.__prevBouncePerSec[2] * 2.0 + self.__prevBouncePerSec[1] * 2.0 + self.__prevBouncePerSec[0] * 2.0 + currBouncePerSec * 4.0)/12.0
		
			activity = 2*bouncePerSec/__MAXBOUNCEPERSEC__ - 1
		
			for n in range(1,len(self.__prevBouncePerSec)):
				self.__prevBouncePerSec[n-1] = self.__prevBouncePerSec[n]
			self.__prevBouncePerSec[3] = currBouncePerSec
			#print self.__prevBouncePerSec
			
			# low activity means messages arrive slower and it takes longer to update....
			if activity > 0:
				valInc = 0.1
			else:
				valInc = 0.2
			# Increase valence if previous wall was the same group, decrease if it was different
			if self.__prevBounce[1] == 1 or self.__prevBounce[1] == 2:
				prevGroup = 0
			else:
				prevGroup = 1
		
			if wall == 1 or wall == 2:
				currGroup = 0
			else:
				currGroup = 1
		
			valence = self.__prevValence
			if currGroup == prevGroup:
				if valence < 1.0:
					valence += valInc
			else:
				if valence > -1.0:
					valence -= valInc
			
			#print activity,valence
		

			"""print self.__wallCount
			# Valence is the difference between the sum of bounces on opposite walls
			#valence = (float(abs(self.__wallCount[0] + self.wallCount[2] - self.__wallCount[1] - self.wallCount[3]))-(__BOUNCEBUFFERSIZE__/2.0))/(__BOUNCEBUFFERSIZE__/2.0)
			valence = (float(abs(self.__wallCount[0] - self.__wallCount[1]))-__BOUNCEBUFFERSIZE__)/__BOUNCEBUFFERSIZE__"""
		
		
			# Check if out of bounds
			if activity > 0.999:
				activity = 0.999
			elif activity < -0.999:
				activity = -0.999
			if valence > 0.999:
				valence = 0.999
			elif valence < -0.999:
				valence = -0.999
				

		
			# Save current state
			self.__prevBounce = [bounce,wall]
			#self.__prevBouncePerSec = bouncePerSec
			self.__prevValence = valence
			
			activity = activity * 0.8
			valence = valence * 0.8
		
			# Emit signals
			if not self.__playrecState:
				self.emit(QtCore.SIGNAL("OSCactivity(double)"),float(activity))
				self.emit(QtCore.SIGNAL("OSCvalence(double)"),float(valence))
		else: # Filter the microbounces on the same wall
			pass
	
	def recvAcc(self,*msg):
		"""
		Receive accelerometer message, fill a buffer and analyze them, send SIGNALS
		"""
		#print msg[0]			
		self.__activeBuffer.put(msg[0])
		if self.__logacc:
			self.__logfile.write(str(msg[0][2])+','+str(msg[0][3])+','+str(msg[0][4])+'\n')
		if self.__activeBuffer.full():
			#print str(msg[0][2])+','+str(msg[0][3])+','+str(msg[0][4])+'\n'
			tmp = self.__activeBuffer
			self.__activeBuffer = self.__fullBuffer
			self.__fullBuffer = tmp
			#print 'full'
			threading.Thread(target = self.__accAnalysis).start()
				
	def recvAccBeat(self,*msg):
		"""
		Extract IOI from acceloremeter data, as well as fill a buffer to compute the quantity of motion
		"""
		x = float(msg[0][2])
		y = float(msg[0][3])
		z = float(msg[0][4])

		if msg[0][5] == 1:
		
			#print str(x)+','+str(y)+','+str(z)
			"""acc = (pow(z,2)-0.22)*1.28
			if acc>self.__tempAcc:
				print acc
				self.__tempAcc = acc
			"""
			#print z
			onset = time() - self.__timeOffset
			#print onset
			if z < self.__onThr and not self.__onState:
				#print y
				notesPerSec = self.__tempo/(onset - self.__prevOnset)
				if notesPerSec < 3:				
					print 'ON ' + str(onset - self.__prevOnset)
					self.__onState = True
					# Compute the IOI
					self.__prevOnset = onset
					self.__currentNotesPerSec = notesPerSec
					#print str(notesPerSec)
					self.emit(QtCore.SIGNAL("OSCnPerSec(double)"),float(notesPerSec))
			#elif z > -0.3 and self.__onState:
			elif z > self.__onThr and self.__onState:
				#notesPerSec = self.__tempo/(onset - self.__prevOnset)
				#if notesPerSec < 2:
				#	print 'OFF'
				#	self.__onState = False
				self.__onState = False
				print "OFF"
		
		elif msg[0][5] == 2:
			#acc = pow(x,2) + pow(y,2) + pow(z,2) - 18 #correct for G
			acc = pow(z,2)
			self.__activeBuffer.put(acc)
			if self.__activeBuffer.full():
			#print str(msg[0][2])+','+str(msg[0][3])+','+str(msg[0][4])+'\n'
				accBuffer = self.__activeBuffer
				self.__activeBuffer = self.__fullBuffer
				self.__fullBuffer = accBuffer
				#print 'full'
				threading.Thread(target = self.__accAnalysisValence).start()
		else: pass
	
			
		# Send  out amplitude data:
		#dyna = 3.0*(acc/50.0-1)
		#if dyna > 3.0:
		#	dyna = 3.0
		#elif dyna< -3.0:
		#	dyna = -3.0
		#print str(dyna)
		#self.emit(QtCore.SIGNAL("OSCdyna(double)"),float(dyna))

	def __accAnalysisValence(self):
		"""
		Compute the quantity of motion
		Estimate Valence using some simple logic
		"""
		#print 'hello'
		vect = array.array('f')
		accum = 0
		# Read from the buffer and compute the squared modulus of the acceleration
		while not self.__fullBuffer.empty():
			acc = self.__fullBuffer.get()
			accum += float(acc)
		acc_avg = accum/float(self.__bufferSize)
		#print acc_avg
		dyna = 10*((2*(acc_avg-0.2)/0.7)-1) # Sub-optimal normalization,.,,
		self.emit(QtCore.SIGNAL("OSCdyna(double)"),float(dyna))
		"""#dyna = (acc_avg/30.0-1)
		#print dyna
		A = 0
		H = 0
		S = 0
		T = 0
		
		# Use  tempo to determine if we are in positive or negative activity
		activity = self.__currentNotesPerSec
		
		if activity > 1:
			A =+ 0.25
			H =+ 0.25
		else:
			S =+ 0.25
			T =+ 0.25
		
		
		#if  activity > 1.25: 
		#	A =+ 0.25
		#	H =+ 0.25
		#elif activity <= 1.25 and activity > 1:
		#	res = (1.25-activity)*0.25/0.25
		#	S =+ res
		#	T =+ res
		#	A =+ 0.25-res
		#	H =+ 0.25-res
		#elif activity <= 1 and activity > 0.75:
		#	res = (activity - 0.75) * 0.25/0.25
		#	S =+ 0.25 - res
		#	T =+ 0.25 - res
		#	A =+ res
		#	H =+ res
		#else:
		#	S =+ 0.25
		#	T =+ 0.25
		#print A,H,T,S
		#print dyna
		# Use dynamics
		if dyna > 1:
			A =+ 0.5
		elif dyna > 0.75 and dyna <= 1:
			res = (1-dyna)*0.5/0.25
			A =+ 0.5 - res
			H =+ res
		elif dyna > 0.25 and dyna <= 0.75:
			H =+ 0.5
		elif dyna > -0.25 and dyna<= 0.25:
			res = (dyna+0.25)*0.5/0.25
			H =+ 0.5 - res
			T =+ res
		elif dyna > -0.75 and dyna <= -0.25:
			T =+ 0.5
		elif dyna > -1 and dyna <= -0.75:
			res = (dyna+1)*0.5/0.25
			T =+ 0.5 - res
			S =+ res
		else:
			S =+ 0.5

		#print A,H,T,S"""
		
		# Find the max and subtract the opposite emotion
		"""valence = max([A,H,T,S])
		if valence == A:
			valence = -2*A
			print 'Angry'
		elif valence == S:
			valence = -2*S
			print 'Sad'
		elif valence == H:
			valence = 2*H
			print 'Happy'
		else:
			valence = 2*T
			print 'Tender'
		print valence

		#if valence == A:
		#	valence = -(A - H)
		#elif valence == H:
	    #	valence = H - A
		#elif valence == T:
		#	valence = T - S
		#else:
		#	valence = -(S - T)
		
		# Convert activity from ratio to linear
		
		# Check if out of bounds
		if activity > 0.999:
			activity = 0.999
		elif activity < -0.999:
			activity = -0.999
		
		if valence > 0.999:
			valence = 0.999
		elif valence < -0.999:
			valence = -0.999"""
		
		#print str(activity)+','+str(valence)

		
		#print str(dyna)
		
		
		
		#self.emit(QtCore.SIGNAL("OSCvalence(double)"),float(valence))
		#self.emit(QtCore.SIGNAL("OSCactivity(double)"),float(activity))
		
	def __accAnalysis(self):
		"""
		Uses autocorrelation to analyze the gestures, and outputs periodicity and amplitude
		"""
		#print 'hello'
		vect = array.array('f')
		accum = 0
		# Read from the buffer and compute the squared modulus of the acceleration
		while not self.__fullBuffer.empty():
			msg = self.__fullBuffer.get()
			mod = 0
			for i in range(2,len(msg)):
				mod += pow(float(msg[i]),2)
			vect.append(mod)
			accum += mod
		vect_avg = accum/len(vect)
		
		# Subtract the mean
		for i in range(0,len(vect)):
			vect[i] = vect[i]-vect_avg
		
		c = autocorr(self.__oldValues+vect,self.__lags)/self.__lags
		c = c.tolist()
	
		# Extract the peak at i = 0 --> activity (scale between -1:1)
		activity = 2*(c[0]/__FACTOR__)-1
		# FIXME! This is divided by the max value that can be reached for a signal with range +-2 (that is, RMS^2 = a^2 /2 )
	
		# Find the periodicity (i.e. ratio between c[0] and the second peak)
		# Look for the first and second zero-crossing and then find the maximum between those two
		try:
			zc1 = 0
			i = 0
			while not zc1 and i<self.__lags:
				if c[i]<0:
					zc1 = i
				else:
					i += 1
			zc2 = 0
			i = zc1
			while not zc2 and i<self.__lags:
				if c[i]>0:
					zc2 = i
				else:
					i +=1
			
			i = zc2
			#zc3 = 0
			#while not zc3 and i<self.__lags:
			#	if c[i]<0:
			#		zc3 = i
			#	else:
			#		i +=1
			#peak2 = c.index(max(c[zc2:zc3])) # find index of the maximum. we know that the first maxima is at index 0, we need to find the second... 
			#print zc2
			peak2 = c.index(max(c[zc2:self.__lags])) # find index of the maximum.
			
			#print c[peak2]/c[0]
			#periodicity = 2*(c[peak2]/c[0])-0.8
		
			# TEST: variation in periodicity amplitude!
			self.__r[0] = self.__r[1]
			self.__r[1] = c[peak2]/c[0]
			self.__deltaA[1] = 1 - abs((self.__r[1]-self.__r[0])/self.__r[1])
			periodicity = self.__w[0] * self.__deltaA[0] + self.__w[1] * self.__deltaA[1]
			
			# Test: variation in period
			#self.__r[0] = self.__r[1]
			#self.__r[1] = peak2
			#self.__deltaA[1] = 1 - (float(abs(self.__r[1]-self.__r[0]))/float(self.__lags))
			#periodicity = self.__w[0] * self.__deltaA[0] + self.__w[1] * self.__deltaA[1]
			#print self.__deltaA[1]
			#periodicity = float(peak2)/float(self.__lags)
			
		except:
			periodicity = 0.5
			print "Error: periodicity estimation"
		periodicity = periodicity*2-1
		print periodicity
		#print periodicity
		# Find period: need fps!
		#period = fps/peak2
		#print period
		#print activity , periodicity
		#print self.__oldValues
		self.__oldValues = vect
		#print self.__oldValues
		#period = 0
		#if self.__results.full():
		#	print self.getNextResult()
		#self.__results.put([activity,periodicity,period])
		self.emit(QtCore.SIGNAL("OSCx(double)"),float(activity))
		self.emit(QtCore.SIGNAL("OSCy(double)"),float(periodicity))
		#print periodicity
		
if __name__ == '__main__':
	# START OSC listening
	daemon = OSCDaemon()
	daemon.startOSC(8000,'127.0.0.1',9000,'/accdata',[0,1],[0,1])
	sleep(20)
	daemon.stopOSC()