#!usr/bin/env/ python
# pyDM : python pdm player and performance control
# Copyright (c) Marco Fabiani 2008
#
# pyDMavspace.py - Dialog to control the activity valence and to manage OSC messages


from PyQt4 import QtCore
from PyQt4 import QtGui
import controlspace
import osc

MAC = "qt_mac_set_native_menubar" in dir()
class avSpace(QtGui.QDialog):
	
	def __init__(self,parent = None):
		"""
		Create the dialog.
		"""
		super(avSpace,self).__init__(parent)
		#self.avSpace = controlspace.controlSpace('','')
		self.avSpace = controlspace.controlSpace('','','','','','')
		self.buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Close|QtGui.QDialogButtonBox.Reset,QtCore.Qt.Vertical)
		layout = QtGui.QHBoxLayout()
		layout.addWidget(self.avSpace)
		layout.addWidget(self.buttonBox)
			
		self.setLayout(layout)		
		self.connect(self.buttonBox,QtCore.SIGNAL("rejected()"),self.reject)
		self.connect(self.buttonBox,QtCore.SIGNAL("clicked(QAbstractButton*)"),self.reset)
		self.connect(self.avSpace,QtCore.SIGNAL("xChanged(double)"),self.activityChanged)
		self.connect(self.avSpace,QtCore.SIGNAL("yChanged(double)"),self.valenceChanged)
		
	def reset(self,button):
		"""
		Reset the values to deadpan
		"""
		if self.buttonBox.buttonRole(button) == QtGui.QDialogButtonBox.ResetRole:
			#self.avSpace.setX(0)
			#self.avSpace.setY(0)
			self.avSpace.reset()
			self.emit(QtCore.SIGNAL("reset()"))
			
	def activityChanged(self,value):
		"""
		Signal a change in activity
		"""
		self.emit(QtCore.SIGNAL("activityChanged(double)"),value)

	def valenceChanged(self,value):
		"""
		Signal a change in valence
		"""
		self.emit(QtCore.SIGNAL("valenceChanged(double)"),value)
	
	def setActivity(self,activity):
		"""
		Set the activity value (slot)
		"""
		self.avSpace.setX(activity)
	
	def setValence(self,valence):
		"""
		Set the valence value (slot)
		"""
		self.avSpace.setY(valence)
		
	def close(self):
		"""Kills the threads
		"""
		self.avSpace.close()
					
if __name__ == "__main__":
	import sys
	app = QtGui.QApplication(sys.argv)
	form = avSpace()
	form.show()
	app.exec_()