#!usr/bin/env/ python
# pyDM : python pdm player and performance control
# Copyright (c) Marco Fabiani 2008
#
# pyDMsettingsdlg.py - Settings dialog (MIDI and OSC)


from PyQt4 import QtCore,QtGui
import ui_settingsdlg
import midiplayer
import copy

MAC = "qt_mac_set_native_menubar" in dir()
class SettingsDlg(QtGui.QDialog,ui_settingsdlg.Ui_settingsDLG):
	
	def __init__(self,midi,midiSettings,oscSettings,parent = None):
		"""
		Create the dialog
		"""
		super(SettingsDlg,self).__init__(parent)
		self.setupUi(self)
		self.midiPlayer = midi
		self.listDevices = []
		self.currentIndex = 0
		
		# make a copy in case of cancel
		self.tmpMidiSettings = copy.copy(midiSettings)
		self.tmpOscSettings = copy.copy(oscSettings)
		#tempcorners = ','.join([str(s) for s in self.tmpOscSettings['corners']])
		#self.tmpOscSettings['corners'] = tempcorners
		tempXRange = ','.join([str(s) for s in self.tmpOscSettings['xRange']])
		#print tempXRange
		self.tmpOscSettings['xRange'] = tempXRange
		
		tempYRange = ','.join([str(s) for s in self.tmpOscSettings['yRange']])
		self.tmpOscSettings['yRange'] = tempYRange		
		
		# the real variable
		self.midiSettings = midiSettings
		self.oscSettings = oscSettings
		
		#if not MAC: 
		#	self.findButton.setFocusPolicy(Qt.NoFocus) 
		#	self.replaceButton.setFocusPolicy(Qt.NoFocus) 
		#	self.replaceAllButton.setFocusPolicy(Qt.NoFocus) 
		#	self.closeButton.setFocusPolicy(Qt.NoFocus)
		
		# Populate the midi devices list:
		self.latencyValue.setText(str(self.tmpMidiSettings['latency']))
		self.listDevices = self.midiPlayer.listOutDevices()
		
		if len(self.listDevices)>0:
			for key in self.listDevices:
				self.midiDeviceList.insertItem(-1,key)
			self.midiDeviceList.setCurrentIndex(0)
			if self.midiSettings['device'] is not None:
			# search if the device is in the list, otherwise set to the first one
				n = self.midiDeviceList.count()
				while n > 0:
					n = n-1
					self.midiDeviceList.setCurrentIndex(n)
					self.currentIndex = n
					if self.midiSettings['device'] == str(self.midiDeviceList.currentText()):
						break
		self.tmpMidiSettings['device']=str(self.midiDeviceList.currentText())
		
		# load OSC settings
		self.oscPortInValue.setText(str(self.tmpOscSettings['portIn']))
		self.oscAddrOutValue.setText(str(self.tmpOscSettings['ipAddrOut']))
		self.oscPortOutValue.setText(str(self.tmpOscSettings['portOut']))
		#self.oscActivityValue.setText(str(self.tmpOscSettings['x']))
		#self.oscValenceValue.setText(str(self.tmpOscSettings['y']))
		self.oscActValValue.setText(str(self.tmpOscSettings['xy']))
		#self.oscCornersValue.setText(str(self.tmpOscSettings['corners']))
		self.oscXRangeValue.setText(str(self.tmpOscSettings['xRange']))
		self.oscYRangeValue.setText(str(self.tmpOscSettings['yRange']))
		self.oscAccBuffValue.setText(str(self.tmpOscSettings['buffSize']))
				
		# connections
		self.connect(self.settingsButtonBox,QtCore.SIGNAL("accepted()"),self.accept)
		self.connect(self.settingsButtonBox,QtCore.SIGNAL("rejected()"),self.cancel)
		self.connect(self.midiDeviceList,QtCore.SIGNAL("activated(int)"),self.deviceChange)
		
		self.connect(self.latencyValue,QtCore.SIGNAL("textChanged(const QString &)"),self.latencyChange)
		
		#self.connect(self.oscActivityValue,QtCore.SIGNAL("textChanged(const QString &)"),self.oscActivityChange)
		
		#self.connect(self.oscValenceValue,QtCore.SIGNAL("textChanged(const QString &)"),self.oscValenceChange)	
		
		self.connect(self.oscActValValue,QtCore.SIGNAL("textChanged(const QString &)"),self.oscActValChange)
		
		#self.connect(self.oscCornersValue,QtCore.SIGNAL("textChanged(const QString &)"),self.oscCornersChange)	
		self.connect(self.oscXRangeValue,QtCore.SIGNAL("textChanged(const QString &)"),self.oscXRangeChange)
		self.connect(self.oscYRangeValue,QtCore.SIGNAL("textChanged(const QString &)"),self.oscYRangeChange)
		
		self.connect(self.oscPortInValue,QtCore.SIGNAL("textChanged(const QString &)"),self.oscPortInChange)
		
		self.connect(self.oscAddrOutValue,QtCore.SIGNAL("textChanged(const QString &)"),self.oscAddrOutChange)
		
		self.connect(self.oscPortOutValue,QtCore.SIGNAL("textChanged(const QString &)"),self.oscPortOutChange)
		
		self.connect(self.oscAccBuffValue,QtCore.SIGNAL("textChanged(const QString &)"),self.oscBuffSizeChange)
		
		# change focus of the tabs
		x = self.settingsTab.setCurrentIndex(0)
		
	def deviceChange(self,id):
		"""
		Set the midi device
		"""
		self.tmpMidiSettings['device'] = str(self.midiDeviceList.currentText())
		
	def accept(self):
		"""
		Save the changes
		"""
		self.midiSettings['device'] = self.tmpMidiSettings['device']
		self.midiSettings['latency'] = self.tmpMidiSettings['latency']
		#self.oscSettings['x'] = self.tmpOscSettings['x']
		#self.oscSettings['y'] = self.tmpOscSettings['y']
		self.oscSettings['xy'] = self.tmpOscSettings['xy']
		
		#corners = [float(s) for s in self.tmpOscSettings['corners'].split(',')]
		#self.oscSettings['corners'] = corners

		xRange = [float(s) for s in self.tmpOscSettings['xRange'].split(',')]
		self.oscSettings['xRange'] = xRange

		yRange = [float(s) for s in self.tmpOscSettings['yRange'].split(',')]
		self.oscSettings['yRange'] = yRange
		self.oscSettings['buffSize'] = self.tmpOscSettings['buffSize']
		
		self.oscSettings['portIn'] = self.tmpOscSettings['portIn']		
		self.oscSettings['ipAddrOut'] = self.tmpOscSettings['ipAddrOut']
		self.oscSettings['portOut'] = self.tmpOscSettings['portOut']
		self.midiPlayer.close()
		if self.midiSettings['device'] is not None:
			self.midiPlayer.start(self.listDevices[self.midiSettings['device']],self.midiSettings['latency'])
		self.done(1)
	
	def cancel(self):
		"""
		Cancel the changes
		"""
		# remove the changes
		self.latencyValue.setText(str(self.midiSettings['latency']))
		self.oscPortInValue.setText(str(self.oscSettings['portIn']))
		self.oscAddrOutValue.setText(str(self.oscSettings['ipAddrOut']))
		self.oscPortOutValue.setText(str(self.oscSettings['portOut']))
		#self.oscActivityValue.setText(str(self.oscSettings['x']))
		#self.oscValenceValue.setText(str(self.oscSettings['y']))
		self.oscActValValue.setText(str(self.oscSettings['xy']))
		self.oscAccBuffValue.setText(str(self.oscSettings['buffSize']))
		
		#tempcorners = ','.join([str(s) for s in self.tmpSettings['corners']])
		#self.oscCornersValue.setText(str(tempcorners))
		
		tempXRange = ','.join([str(s) for s in self.oscSettings['xRange']])
		self.oscXRangeValue.setText(str(tempXRange))
		
		tempYRange = ','.join([str(s) for s in self.oscSettings['yRange']])
		self.oscYRangeValue.setText(str(tempYRange))
		
		self.midiDeviceList.setCurrentIndex(self.currentIndex)
		self.reject()
		
	def	oscActivityChange(self,newtext):
		"""
		Update osc activity message address
		"""
		self.tmpOscSettings['x'] = str(newtext)
		
	def oscValenceChange(self,newtext):
		"""
		Update osc valence message address
		"""
		self.tmpOscSettings['y'] = str(newtext)
		
	def oscActValChange(self,newtext):
		"""
		Update osc valence message address
		"""
		self.tmpOscSettings['xy'] = str(newtext)

	#def oscCornersChange(self,newtext):
	#	"""
	#	Update osc valence message address
	#	"""
	#	
	#	self.tmpOscSettings['corners'] = str(newtext)

	def oscXRangeChange(self,newtext):
		"""
		Update osc valence message address
		"""
		
		self.tmpOscSettings['xRange'] = str(newtext)

	def oscYRangeChange(self,newtext):
		"""
		Update osc valence message address
		"""
		
		self.tmpOscSettings['yRange'] = str(newtext)
		
	def oscAddrOutChange(self,newtext):
		"""
		Update osc IP address
		"""
		self.tmpOscSettings['ipAddrOut'] = str(newtext)
	
	def oscPortOutChange(self,newtext):
		"""
		Update osc port
		"""
		self.tmpOscSettings['portOut'] = int(newtext)
	
	def oscPortInChange(self,newtext):
		"""
		Update osc port
		"""
		self.tmpOscSettings['portIn'] = int(newtext)
		
	def oscBuffSizeChange(self,newtext):
		"""
		Update buffer size
		"""
		self.tmpOscSettings['buffSize'] = int(newtext)
		
	def latencyChange(self,newtext):
		"""
		Update midi latency
		"""
		self.tmpMidiSettings['latency'] = int(newtext)

		
		
if __name__ == "__main__":
	import sys
	app = QApplication(sys.argv)
	midiSettings = {'latency':20,'device':None}
	oscSettings = {'x':'pydm_activity','y':'pydm_valence'}
	midi = midiplayer.midiPlayer()
	form = SettingsDlg(midi,midiSettings,oscSettings)
	form.show()
	app.exec_()