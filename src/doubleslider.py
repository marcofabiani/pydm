#!/usr/bin/env python

#################################################################
#																#
# Custom widget pyQDoubleSlider     							#
#																#
# Subclass of Slider that works with doubles instead of ints	#
# It creates a slider with at most 2 decimals					#	
#################################################################

from PyQt4 import QtCore,QtGui
import sys

class myDoubleSlider(QtGui.QSlider):
	
	__pyqtSignals__ = ("valueDoubleChanged(double)","maxDoubleChanged(double)","minDoubleChanged(double)","stepDoubleChanged(double)","stepPageDoubleChanged(double)","resolutionChanged(double)")
	
	def __init__(self,parent = None):
		
		QtGui.QSlider.__init__(self,parent)
		self.res = 100.0
		self.minD = float(self.minimum())/self.res
		self.maxD = float(self.maximum())/self.res
		self.singleStepD = float(self.singleStep())/self.res
		self.pageStepD = float(self.pageStep())/self.res
		self.valueD = float(self.value())/self.res
		self.connect(self,QtCore.SIGNAL("sliderMoved(int)"),self.sliderChanged)
		self.connect(self,QtCore.SIGNAL("valueChanged(int)"),self.updateValueD)
		self.setOrientation(QtCore.Qt.Horizontal)

	def sliderChanged(self,value):
		self.valueD = float(value)/self.res
		self.emit(QtCore.SIGNAL("valueDoubleChanged(double)"),self.valueD)

	def updateValueD(self,value):
		self.valueD = float(value)/self.res
		self.emit(QtCore.SIGNAL("valueDoubleChanged(double)"),self.valueD)
		
	def updateMaxD(self,value):
		self.maxD = float(value)/self.res
		self.emit(QtCore.SIGNAL("maxDoubleChanged(double)"),self.maxD)
		
	def updateMinD(self,value):
		self.minD = float(value)/self.res
		self.emit(QtCore.SIGNAL("minDoubleChanged(double)"),self.minD)
		
	def updateSingleStepD(self,value):
		self.singleStepD = float(value)/self.res
		self.emit(QtCore.SIGNAL("stepDoubleChanged(double)"),self.singleStepD)		
		
	def updatePageStepD(self,value):
		self.pageStepD = float(value)/self.res
		self.emit(QtCore.SIGNAL("stepPageDoubleChanged(double)"),self.pageStepD)		

	
	@QtCore.pyqtSignature("setValueDouble(double)")
	def setValueDouble(self,value = None):
		self.valueD = value
		self.setValue(int(round(value*self.res)))
		self.emit(QtCore.SIGNAL("valueDoubleChanged(double)"),value)
		
	def resetValueDouble(self):
		self.valueD = float(self.value())/self.res
		self.emit(QtCore.SIGNAL("valueDoubleChanged(double)"),value)		

	def getValueDouble(self):
		return self.valueD
		
	valueDouble = QtCore.pyqtProperty("double", fget=getValueDouble, fset=setValueDouble, freset=resetValueDouble)


	@QtCore.pyqtSignature("setMaxDouble(double)")
	def setMaxDouble(self,value = None):
		self.maxD = value
		self.setMaximum(int(round(value*self.res)))
		self.emit(QtCore.SIGNAL("maxDoubleChanged(double)"),value)
		
	def resetMaxDouble(self):
		self.maxD = float(self.maximum())/self.res
		self.emit(QtCore.SIGNAL("maxDoubleChanged(double)"),value)		

	def getMaxDouble(self):
		return self.maxD
		
	maxDouble = QtCore.pyqtProperty("double", fget=getMaxDouble, fset=setMaxDouble, freset=resetMaxDouble)
						

	@QtCore.pyqtSignature("setMinDouble(double)")
	def setMinDouble(self,value = None):
		self.minD = value
		self.setMinimum(int(round(value*self.res)))
		self.emit(QtCore.SIGNAL("minDoubleChanged(double)"),value)
		
	def resetMinDouble(self):
		self.minD = float(self.minimum())/self.res
		self.emit(QtCore.SIGNAL("minDoubleChanged(double)"),value)		

	def getMinDouble(self):
		return self.minD
		
	minDouble = QtCore.pyqtProperty("double", fget=getMinDouble, fset=setMinDouble, freset=resetMinDouble)

	@QtCore.pyqtSignature("setStepDouble(double)")
	def setStepDouble(self,value = None):
		self.singleStepD = value
		self.setSingleStep(int(round(value*self.res)))
		self.emit(QtCore.SIGNAL("valueStepChanged(double)"),value)
		
	def resetStepDouble(self):
		self.singleStepD = float(self.singleStep())/self.res
		self.emit(QtCore.SIGNAL("stepDoubleChanged(double)"),value)		

	def getStepDouble(self):
		return self.singleStepD
		
	singleStepDouble = QtCore.pyqtProperty("double", fget=getStepDouble, fset=setStepDouble, freset=resetStepDouble)


	@QtCore.pyqtSignature("setPageStepDouble(double)")
	def setPageStepDouble(self,value = None):
		self.pageStepD = value
		self.setPageStep(int(round(value*self.res)))
		self.emit(QtCore.SIGNAL("pageStepDoubleChanged(double)"),value)
		
	def resetPageStepDouble(self):
		self.pageStepD = float(self.value())/self.res
		self.emit(QtCore.SIGNAL("pageStepDoubleChanged(double)"),value)		

	def getPageStepDouble(self):
		return self.pageStepD
		
	pageStepDouble = QtCore.pyqtProperty("double", fget=getPageStepDouble, fset=setPageStepDouble, freset=resetPageStepDouble)
								
	@QtCore.pyqtSignature("setResolution(double)")
	def setResolution(self,value = None):
		self.res = value
		self.maxD = float(self.maximum())/self.res
		self.minD = float(self.minimum())/self.res
		self.valueD = float(self.value())/self.res
		self.pageStepD = float(self.pageStep())/self.res
		self.singleStepD = float(self.singleStep())/self.res		
		self.emit(QtCore.SIGNAL("resolutionChanged(double)"),value)
		
	def resetResolution(self):
		self.res = 100.0
		self.maxD = float(self.maximum())/self.res
		self.minD = float(self.minimum())/self.res
		self.valueD = float(self.value())/self.res
		self.pageStepD = float(self.pageStep())/self.res
		self.singleStepD = float(self.singleStep())/self.res
		self.emit(QtCore.SIGNAL("resolutionDoubleChanged(double)"),value)		

	def getResolution(self):
		return self.res
		
	resolution = QtCore.pyqtProperty("double", fget=getResolution, fset=setResolution, freset=resetResolution)

			
if __name__ == "__main__":

    import sys

    app = QtCore.QApplication(sys.argv)
    slider = myDoubleSlider()
    slider.show()
    sys.exit(app.exec_())		
	
		