#!usr/bin/env/ python
# pyDM : python pdm player and performance control
# Copyright (c) Marco Fabiani 2008
#
# midiplayer.py 
#
# Class that manages MIDI playback on a low level. This is the PORTMIDI version

import time, os, sys, pypm
#import threading, threadpool


class midiPlayer:
	
	global __POLY__ 
	__POLY__ = 32 # number of notes that can be played  simultaneously
		
	def __init__(self):
		"""
		Initialization: create a threadpool with POLY threads
		"""
		pypm.Initialize()
		# create threadpool
		#self.notesPool = threadpool.ThreadPool(__POLY__)	
		self.activeNotes = 0
		self.midiOut = None
		
	def __del__(self):
		"""
		Destroy the portmidi instance
		"""
		pypm.Terminate()
		
	def listOutDevices(self):
		"""
		Outputs a list of midi output devices currently available
		"""
		outList = {} # The name of the device is the key, the ID is the value
		for loop in range(pypm.CountDevices()):
			interf,name,inp,outp,opened = pypm.GetDeviceInfo(loop)
			if (outp == 1):
				outList[name] = loop
		return outList
        
	def start(self,dev,latency = 0):
		"""
		Open the MIDI output device
		@dev: int (ID)
		@latency: int (default 0 ms)
		"""
		self.midiOut = pypm.Output(int(dev),int(latency))

	def playerDone(self):
		"""
		Checks if all the notes have been processed
		"""
		return self.activeNotes == 0
			
	def programChange(self,ch): pass
	
	def volumeChange(self,vol): pass
	
	def getMidiTime(self):
		"""
		Returns the midi time stamp (see portmidi)
		"""
		return pypm.Time()
	
	def close(self):
		"""
		Close the midi out device
		"""
		if self.midiOut is not None:
			del self.midiOut

	#def noteOn(self,note,vel,dur):		
	def noteOn(self,ch,note,vel,dur):
		"""
		Sends out a midi note on message and updates the active notes count.
		"""
		self.activeNotes +=1
		if vel>127:
			vel = 127
		elif vel < 10:
			vel = 10
		#print vel
		self.midiOut.Write([[[0x90+ch-1,note,vel],pypm.Time()]]) # note on message

	def noteOff(self,ch,note):		
	#def noteOff(self,note):
		"""
		Sends out a midi note off message and updates the active notes count
		"""
		self.midiOut.WriteShort(0x90+ch-1,note,0) # note off message
		self.activeNotes -=1
		
	def programChange(self,ch,prog):
		"""
		Change program on channel ch to prog
		"""
		self.midiOut.WriteShort(0xC0+ch-1,prog-1) # program change message
		
	def channelVolume(self,ch,vol):
		"""
		Change the overall volume for a channel
		"""
		self.midiOut.WriteShort(0xB0+ch-1,0x07,vol-1) # Control change message, volume
		
	def sustainPedal(self,val):
		"""
		Send control MIDI message for sustain  pedal
		"""
		self.midiOut.WriteShort(0xB0,0x40,val)
	
	def softPedal(self,val):
		"""
		Send control MIDI message for soft pedal
		"""
		self.midiOut.WriteShort(0xB0,0x43,val)
		