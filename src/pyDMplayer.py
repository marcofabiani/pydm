#!usr/bin/env/ python
# pyDM : python pdm player and performance control
# Copyright (c) Marco Fabiani 2008
#
# pyDMplayer.py - Player class (secondary thread)


import time
import PyQt4.QtCore

class Player(PyQt4.QtCore.QThread):
	
	#Constants here
	global SEEK_STEP
	SEEK_STEP = 1
	
	def __init__(self,lock,parent = None):
		"""
		Create the secondary thread.
		@lock: QtCore.QReadWriteLock(), used to manage variable in common between the main thread and the secondary thread
		"""
		super(Player,self).__init__(parent)
		self.cmLock = lock
		self.closed = False # When true, close the secondary thread as soon as possible
		self.mutex = PyQt4.QtCore.QMutex() # protect the stopped variable
		
	def initialize(self,performance,k,overall,xy): 
		"""
		Called just before start() to "load" the performance player.
		@performance: perplayer.PerfPlayer()
		@k: vector of doubles (19)
		@overall: vector of doubles (2)
		@xy: [x,y,flag], activity valence coordinates
		"""
		self.closed = False
		self.performance = performance
		self.cmKVector = k # Common variable with the main window
		self.cmOverallScaling = overall # Common variable
		self.cmXY = xy
		self.autoPerformance = False # defines if we are playing in auto mode
		self.playing = False
		self.repeating = False
		
	def run(self):
		"""
		Overriden function from QThread (run)
		"""
		self.perform()
		self.terminate()
		self.emit(PyQt4.QtCore.SIGNAL("terminated"))	
		
	def terminate(self):
		"""
		Overriden function from QThread (terminate)
		"""
		try: 
			self.mutex.lock() 
			self.closed = True 
		finally: 
			self.mutex.unlock()
			
	def isClosed(self):
		"""
		Check if self.closed == TRUE. In that case, try to close the thread as soon as possible.
		"""
		try: 
			self.mutex.lock() 
			return self.closed 
		finally: 
			self.mutex.unlock()
			
	def isPlaying(self): 
		"""
		Check if the player is playing.
		"""
		try: 
			self.mutex.lock() 
			return self.playing 
		finally: 
			self.mutex.unlock()		
			
	def perform(self):
		"""
		Main loop for the secondary thread. As long as player has not been asked to exit, keep looping.
		"""
		while not self.isClosed(): #check if the player has been closed
			if self.isPlaying():
				# Decide who controls the performance (score or user)
				if not self.autoPerformance: # "performance mode", use the values from the interface
					# send performance information, lock first!
					try:
						#print self.cmXY
						self.cmLock.lockForWrite()
						self.performance.updateXY(self.cmXY)
						# set flag to 0 after passing the value
						self.performance.updateKVec(self.cmKVector)
						self.performance.updateOverall(self.cmOverallScaling)
					finally:
						self.cmLock.unlock()
				else: # read the performance rules from the score and send them to the interface: first check for XY, otherwise dont send the values
					tempXY = self.performance.getXY()
					if tempXY[2]:
						self.emit(PyQt4.QtCore.SIGNAL("xChanged(double)"),tempXY[0])
						self.emit(PyQt4.QtCore.SIGNAL("yChanged(double)"),tempXY[1])
					else:
						tempRules = self.performance.getKVec()
						tempOverall = self.performance.getOverall()
						self.emit(PyQt4.QtCore.SIGNAL("rulesChanged(PyQt_PyObject,PyQt_PyObject)"),tempRules,tempOverall)
						#self.emit(PyQt4.QtCore.SIGNAL("overallChanged(PyQt_PyObject)"),tempOverall)						
					tempK = self.performance.getKVec()
					tempOverall = self.performance.getOverall()
					self.updateCommonVectors(tempK,tempOverall)
					
				# read the next line
				self.performance.readLine()
				bar = self.performance.getBar()
				self.emit(PyQt4.QtCore.SIGNAL("currentBar(int)"),int(bar))
				# increment current position
				self.performance.incPos()
				if self.performance.endFile():
					if self.repeating:
						# Rewind to start
						#print 'repeat'
						self.pause()
						self.performance.rewindToStart()
						#print self.autoPerformance
						self.play(self.autoPerformance)
					else:
						self.stop()
						#print 'stop'
			else:
				time.sleep(0.5)	
				
	def play(self,auto = False): 
		"""
		Start to play. If auto, play automatic performance (recorded)
		"""
		self.autoPerformance = auto
		try: 
			self.mutex.lock() 
			self.playing = True
			# notify the main thread that the player started to play
			self.emit(PyQt4.QtCore.SIGNAL("playing(bool)"), True)
		finally: 
			self.mutex.unlock()
	
	def stop(self):
		"""
		Stop the player.
		"""
		self.pause()
		self.performance.rewindToStart()
		# Notify the main thread at which bar the player stoped.
		self.emit(PyQt4.QtCore.SIGNAL("currentBar(int)"),int(self.performance.getBar()))
			
	def pause(self):
		"""
		Pause the player.
		"""
		try: 
			self.mutex.lock()
			self.performance.allOff(self.performance.endFile()) # stop all active notes
			self.playing = False
			# Notify the main thread that the player has stoped.
			self.emit(PyQt4.QtCore.SIGNAL("playing(bool)"), False)
		finally:
			self.mutex.unlock()
			
	def repeat(self,val=True):
		self.repeating = val
			
	def fastforward(self):
		"""
		Seek the file forward at steps of SEEk_STEP bars
		"""
		status = self.isPlaying()
		self.pause()
		self.performance.jumpToBar(SEEK_STEP)
		self.emit(PyQt4.QtCore.SIGNAL("currentBar(int)"),self.performance.getBar())
		if status:
			self.play() # resume playing
		
	def rewind(self):
		"""
		Seek the file backward at steps of SEEk_STEP bars
		"""
		status = self.isPlaying()
		self.pause()
		self.performance.jumpToBar(-SEEK_STEP)
		self.emit(PyQt4.QtCore.SIGNAL("currentBar(int)"),self.performance.getBar())		
		if status:
			self.play() # resume playing
	
	def record(self,rec):
		"""
		Start/stop the recording. This is just a connection function to perfplayer.
		@rec: boolean
		"""
		self.performance.setRecording(rec)
		
	def updateCommonVectors(self,k,overall):
		"""
		Get the values of k and overall and update the common vectors when reading the performance from the file. Needed because of the lock-unlock system.
		@k: vector of doubles (19)
		@overall: vector of doubles (2)
		"""
		try:
			self.cmLock.lockForWrite()
			for m in range(0,len(k)):
				self.cmKVector[m] = k[m]
		finally:
			self.cmLock.unlock()
		try:
			self.cmLock.lockForWrite()
			self.cmOverallScaling[0] = overall[0]
			self.cmOverallScaling[1] = overall[1]
		finally:
			self.cmLock.unlock()