#!usr/bin/env/ python
# pyDM : python pdm player and performance control
# Copyright (c) Marco Fabiani 2008
#
# pyDM.py - Main application window (main thread)

import sys , os
import platform
import pickle
import ui_mainwindow_avsliders
import pyDMsettingsdlg
import pyDMavSpace
import helpform
import oscDaemon


from PyQt4 import QtCore,QtGui


MAC = "qt_mac_set_native_menubar" in dir()

import pyDMplayer # Secondary thread
import midiplayer # MIDI management class, platform-dependent
import perfplayer  # Score and performance classes

__version__ = "0.7.7"


# Default rule values
__K_DEADPAN__ = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
__KTEMPO_DEADPAN__ = [0,0,0,0,0,0,0,0,0,0,0]
__KART_DEADPAN__ = [0,0,0,0,0]
__KSL_DEADPAN__ = [0,0,0,0,0,0,0,0,0]
__OVERALL_DEADPAN__ = [1,0]

class MainWindow(QtGui.QMainWindow,ui_mainwindow_avsliders.Ui_pdmPlayerMain):

	def __init__(self,parent=None):
		"""
		Initialize pyDM main window: load settings, create settings dialog, activity valence dialog, start the performance player.
		"""
		super(MainWindow,self).__init__(parent)
		
		# recreate the GUI from designer (see page 217 Rapid Gui with python and qt)
		self.setupUi(self)
		
		self.filename = None # open file name
		self.filenameOut = None # file to be saved
		self.fileIsOpen = False # is there a file loaded?
		self.dirty = False # can be used to verify if the performance has been changed since last change
		self.recentFiles = [] # list of recently opened files
		
		# Retrieve settings
		settings = QtCore.QSettings()
		self.recentFiles = settings.value("RecentFiles").toStringList()
		self.restoreGeometry(settings.value("Geometry").toByteArray())
		self.restoreState(settings.value("MainWindow/State").toByteArray())

		midisettings = settings.value("MidiSettings").toString()
		if midisettings:
			self.midiSettings = pickle.loads(str(midisettings))
		else: self.midiSettings = {'latency':0,'device':None}

		oscsettings = settings.value("OscSettings").toString()
		if oscsettings:
			self.oscSettings = pickle.loads(str(oscsettings))
		else: self.oscSettings = {'portIn':8001,'ipAddrOut':'127.0.0.1','portOut':9001,'x':'activity','y':'valence','xy':'act_val','corners': [-1.0,1.0,-1.0,1.0],'xRange': [-1.0,1.0] , 'yRange': [-1.0,1.0],'buffSize':40} # ADD BUFFER SIZE!
			
		self.setWindowTitle("PyDM")
		self.updateFileMenu() # update file menu with the list of recently opened files
		QtCore.QTimer.singleShot(0,self.loadInitialFile)
		
		self.repeat = False # loop the file
		
		# Create the OSC daemon
		#self.oscDaemon = oscDaemon.OSCDaemon(self.oscSettings['buffSize']) # Add buffer size as settings!
		self.oscDaemon = oscDaemon.OSCDaemon() # Add buffer size as settings!
		self.oscAcc = False #Used to choose if to use the accelerometers or the xy plane
		
		# Connect the file menu
		self.connect(self.menuRecentPerformances,QtCore.SIGNAL("aboutToShow()"),self.updateFileMenu)
		
		# common variables (start with cm)
		self.cmLock = QtCore.QReadWriteLock() # common variable space lock
		self.cmKVector = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		#self.cmKTempo = KTEMPO_DEADPAN
		#self.cmKArt = KART_DEADPAN
		#self.cmKSl = KSL_DEADPAN
		self.cmOverallScaling = [1,1]
		# cmXY=[x,y,flag], flag to indicate if the coordinates need to be used on not in performance playback
		self.cmXY = [0,0,0]
		
		# Set up pdm score reader and midi player
		self.midi = midiplayer.midiPlayer() # MIDI player
		self.score = perfplayer.PerfPlayer(self.midi) # new, empty performance object
		self.setScoreInfo()
		# Create secondary thread (player) and initialize, in case of close without opening a file
		self.player = pyDMplayer.Player(self.cmLock, self)
		self.player.initialize(self.score,self.cmKVector,self.cmOverallScaling,self.cmXY)
		self.player.repeat(self.repeat)
		# Check if some midi device is open: if not, emit a warning
		if self.midi.listOutDevices() == {}:
			nodevice = QtGui.QMessageBox.warning(self,"PyDM","No MIDI out device detected.\n Continue without MIDI output?",QtGui.QMessageBox.Yes|QtGui.QMessageBox.No)
			if nodevice == QtGui.QMessageBox.No:
				self.closeEvent()
				sys.exit()
			else: pass
		#############################################
		# create the settings dialog
		self.settingsDlg = pyDMsettingsdlg.SettingsDlg(self.midi,self.midiSettings,self.oscSettings,self)
		if self.midiSettings['device'] is not None and self.midi.listOutDevices() != {}:
			self.settingsDlg.accept()
		##########################################
		# create the activity-valence control space and make connections
		self.avSpace = pyDMavSpace.avSpace()
		#self.avSpace = pyDMavSpace.avSpace()
		self.connect(self.avSpace,QtCore.SIGNAL("activityChanged(double)"),self.activitySlider.setValueDouble)
		self.connect(self.avSpace,QtCore.SIGNAL("valenceChanged(double)"),self.valenceSlider.setValueDouble)
		self.connect(self.avSpace,QtCore.SIGNAL("reset()"),self.resetRules)
		############################################
		# Activity valence sliders
		self.connect(self.activitySlider,QtCore.SIGNAL("valueDoubleChanged(double)"),self.updateActivity)
		self.connect(self.valenceSlider,QtCore.SIGNAL("valueDoubleChanged(double)"),self.updateValence)
		
		self.connect(self.resetButton,QtCore.SIGNAL("clicked()"),self.resetRules)
		
		self.connect(self.openAVSpace,QtCore.SIGNAL("clicked()"),self.avSpace.show)
		self.connect(self.actionOpen_Activity_Valence_Space,QtCore.SIGNAL("triggered()"),self.avSpace.show)
		
		self.connect(self.player,QtCore.SIGNAL("xChanged(double)"),self.avSpace.setActivity)
		self.connect(self.player,QtCore.SIGNAL("yChanged(double)"),self.avSpace.setValence)
		
		########################################################
		# Possible connections for the auto-performance update of the sliders?
		self.connect(self.player,QtCore.SIGNAL("rulesChanged(PyQt_PyObject,PyQt_PyObject)"),self.updateSliders)		
		########################################################
		# make connections for the buttons in the tool bar/menu bar
		self.connect(self.actionOpen_score,QtCore.SIGNAL("triggered()"),self.fileOpenPDM)
		self.connect(self.actionOpen_PDM_score,QtCore.SIGNAL("triggered()"),self.fileOpenPDM)
		self.connect(self.actionSave_performance,QtCore.SIGNAL("triggered()"),self.fileSave)
		self.connect(self.actionSave_performance_as,QtCore.SIGNAL("triggered()"),self.fileSaveAs)
		
		# When opening settings, stop player (note: no signal is emitted,need to change the button status manually)
		self.connect(self.actionSettings,QtCore.SIGNAL("triggered()"),self.scoreStop)
		# This sets the OSC button to not checked, but no signal is emitted from Start_OSC!!!
		self.connect(self.actionSettings,QtCore.SIGNAL("triggered(bool)"),self.actionStart_OSC.setChecked)
		self.connect(self.actionSettings,QtCore.SIGNAL("triggered()"),self.openSettings)
		
		
		self.connect(self.actionClose_file,QtCore.SIGNAL("triggered()"),self.closeFile)
		self.connect(self.actionQuit,QtCore.SIGNAL("triggered()"),self.closeEvent)
		
		self.connect(self.action_Play,QtCore.SIGNAL("triggered(bool)"),self.scorePlayPause)
		self.connect(self.actionStop,QtCore.SIGNAL("triggered()"),self.scoreStop)
		self.connect(self.actionRewind,QtCore.SIGNAL("triggered()"),self.scoreRewind)
		self.connect(self.actionFast_Forward,QtCore.SIGNAL("triggered()"),self.scoreFastForward)
		self.connect(self.actionReplay_Performance,QtCore.SIGNAL("triggered(bool)"),self.perfPlayPause)
		self.connect(self.actionRecord,QtCore.SIGNAL("triggered(bool)"),self.perfRecord)
		self.connect(self.action_Repeat,QtCore.SIGNAL("triggered(bool)"),self.scoreRepeat)
		######################################################
		# OSC Daemon connections:
		## Control commands
		self.connect(self.actionStart_OSC,QtCore.SIGNAL("triggered(bool)"),self.oscStartStop)
		self.connect(self.actionStart_Acc,QtCore.SIGNAL("triggered(bool)"),self.accStart)
		self.connect(self.oscDaemon,QtCore.SIGNAL("OSCPlay(bool)"),self.scorePlayPause)
		self.connect(self.oscDaemon,QtCore.SIGNAL("OSCStop()"),self.scoreStop)
		self.connect(self.oscDaemon,QtCore.SIGNAL("OSCRec(bool)"),self.perfRecord)
		self.connect(self.oscDaemon,QtCore.SIGNAL("OSCRepeat(bool)"),self.scoreRepeat)
		self.connect(self.oscDaemon,QtCore.SIGNAL("OSCAcc(bool)"),self.accStart)
		self.connect(self.oscDaemon,QtCore.SIGNAL("OSCPlayRec(bool)"),self.perfPlayPause)
		self.connect(self.oscDaemon,QtCore.SIGNAL("OSCfile(int)"),self.selectFile)
		
		## Balance the performance mode
		self.connect(self.oscDaemon,QtCore.SIGNAL("OSCx(double)"),self.avSpace.setActivity)
		self.connect(self.oscDaemon,QtCore.SIGNAL("OSCy(double)"),self.avSpace.setValence)
		
		## For the emotion recog
		#self.connect(self.oscDaemon,QtCore.SIGNAL("OSCnPerSec(double)"),self.updateTempoRatio)
		#self.connect(self.oscDaemon,QtCore.SIGNAL("OSCvalence(double)"),self.updateValenceNoTempo)
		#self.connect(self.oscDaemon,QtCore.SIGNAL("OSCactivity(double)"),self.updateActivityNoTempo)
		
		## For the bounce analysis
		self.connect(self.oscDaemon,QtCore.SIGNAL("OSCvalence(double)"),self.avSpace.setValence)
		self.connect(self.oscDaemon,QtCore.SIGNAL("OSCactivity(double)"),self.avSpace.setActivity)
		
		
		self.connect(self.actionAbout,QtCore.SIGNAL("triggered()"),self.showAbout)
		self.connect(self.actionHelp,QtCore.SIGNAL("triggered()"),self.showHelp)
		################################################################
		# make connections for player status
		self.connect(self.player,QtCore.SIGNAL("playing(bool)"),self.playerStatus)
		self.connect(self.player,QtCore.SIGNAL("terminated"),self.playerClosed)
		self.connect(self.player,QtCore.SIGNAL("currentBar(int)"),self.updateBar)
		##################################################################
		# Create connections for rule sliders
		self.connect(self.rule1Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule2Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule3Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule4Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule5Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule6Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule7Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule8Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule9Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule10Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule11Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule12Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule13Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule14Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule15Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule16Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule17Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule18Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.rule19Spinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		
		self.connect(self.tempoSpinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		self.connect(self.dynamicsSpinbox,QtCore.SIGNAL("valueChanged(double)"),self.updateRules)
		
	def loadInitialFile(self):
		"""
		Load the last opened file, if any.
		"""
		settings = QtCore.QSettings()
		fname = unicode(settings.value("LastFile").toString())
		if fname and QtCore.QFile.exists(fname):
			self.loadFile(fname)

	def fileOpen(self):
		"""
		Open an xml score file.
		"""
		if not self.okToContinue():
			return
		if self.filename is not None:
			dir = os.path.dirname(self.filename)
		else:
			dir = os.path.expanduser('~') # user's root directory
		formats = ["*.xml"]
		fname = unicode(QtGui.QFileDialog.getOpenFileName(self,"Choose a performance file ...", dir, "Performance files (%s)" % " ".join(formats)))
		if fname:
			self.loadFile(fname)
			
	
	def fileOpenPDM(self):
		"""
		Open an xml score file.
		"""
		self.scoreStop()
		if not self.okToContinue():
			return
		if self.filename is not None:
			dir = os.path.dirname(self.filename)
		else:
			dir = os.path.expanduser('~')
		formats = ["*.pdm"]
		fname = unicode(QtGui.QFileDialog.getOpenFileName(self,"Choose a PDM file ...", dir, "Performance files (%s)" % " ".join(formats)))
		if fname:
			return self.loadFile(fname)
		else: return False
            
	def loadFile(self, fname=None):
		"""
		Load a score file and print the information.
		@fname: string (default = None)
		"""
		loaded = False
		if fname is None:
			action = self.sender()
			if isinstance(action, QtGui.QAction):
				fname = unicode(action.data().toString())
				if not self.okToContinue():
					return
			else:
				return			
		if fname:
			print fname
			self.filename = None
			if (fname[-4:] == ".pdm") or (fname[-4:] == ".PDM"):
				result = self.score.openPDMScore(fname)
			else:
				result = self.score.openXMLScore(fname)
			if not result:
				message = "Failed to read %s" % fname
			else:
				self.addRecentFile(fname)
				self.dirty = False
                # Set all labels to the correct value
				self.setScoreInfo()
                # Start the secondary thread (player)!!!!
                if self.player.isRunning():
                	self.player.terminate()
                	self.player.wait()
                self.player.initialize(self.score,self.cmKVector,self.cmOverallScaling,self.cmXY)
                self.player.repeat(self.repeat)
                self.player.start()
                message = "Loaded %s" % os.path.basename(fname)
                self.fileIsOpen = True
                loaded = True
           	self.updateStatus(message)
		return loaded
           	
	def fileSave(self):
		"""
		Save the score/performance file: if no file name is available, ask for a file name.
		"""
		if not self.fileIsOpen:
			return
		if self.filename == None:
			self.fileSaveAs() 
		else:
			self.score.savePerformancePDM(self.filename)
			self.addRecentFile(self.filename)
			message = "File saved as %s" % os.path.basename(self.filename)
			self.updateStatus(message)
			self.dirty = False
	
	def fileSaveAs(self):
		"""
		Save score/performance file as ... (pdm format)
		"""
		if not self.fileIsOpen:
			return
		fname = self.filename if self.filename is not None else os.path.expanduser('~')
		formats = ["*.pdm"]
		fname = unicode(QtGui.QFileDialog.getSaveFileName(self,"pyDM - Save performance",fname,"Performance files (%s)" % " ".join(formats)))
		if fname:
			if "." not in fname:
				fname += ".pdm"
			self.filename = fname
			self.fileSave()
			
	def openSettings(self):
		"""
		Initialize the settings dialog.
		"""
		self.accStart(False)
		self.oscStartStop(False)
		res = self.settingsDlg.exec_()
	
	def scorePlayPause(self,checked):
		"""
		Control the play/pause button: if no file is open, promt to open one.
		@checked: boolean
		"""
		self.autoPlay = False
		#Promt to open a file if none is open
		if not self.fileIsOpen:
			if not self.fileOpenPDM():
				self.action_Play.toggle()
				checked = False
		if checked: # i am not playing
			# reset the rule vectors to current position
			self.updateRules()
			self.player.play(False) # start playing in interactive mode (False refers to auto mode)
			self.updateStatus("Playing...",0)
		else: # i am playing
			self.player.pause()
			self.updateStatus("Player in Pause...",0)

	def scoreStop(self):
		"""
		Stop the playback/recording.
		"""
		self.player.stop()
		self.player.record(False)
		self.updateStatus("Player stopped",5000)
	
	def scoreFastForward(self):
		"""
		Seek forward in the score (connection function only).
		"""
		self.player.fastforward()
				
	def scoreRewind(self):
		"""
		Rewind the score (connection function only).
		"""
		self.player.rewind()
	
	def scoreRepeat(self,val):
		self.repeat = val
		self.player.repeat(val)
		self.action_Repeat.setChecked(val)
		
	def perfPlayPause(self,checked):
		"""
		Control the playback of a recorded performance.
		@checked: boolean
		"""
		self.autoPlay = True
		if checked: # i am not playing
			self.player.play(True) # True refers to auto mode)
			self.updateStatus("Playing recorded performance ...",0)
		else:
			self.player.pause()
			self.updateStatus("Player in Pause ...",0)
		self.actionReplay_Performance.setChecked(checked)

	
	def perfRecord(self,checked):
		"""
		Start recording a performance by saving rule parameters vectors and activity-valence coordinates.
		@checked: boolean
		"""
		self.player.record(True)
		self.dirty = True
		if checked:
			self.scorePlayPause(checked) #Start the player
			self.updateStatus("Recording ...",0)
		else:
			self.scoreStop()
		self.actionRecord.setChecked(checked)

	def oscStartStop(self,checked):
		"""
		Start/Stop receiving data from the X and Y OSC open ports.
		@checked: boolean
		"""
		if checked:
			self.oscDaemon.startOSC(self.oscSettings['portIn'],self.oscSettings['ipAddrOut'],self.oscSettings['portOut'],self.oscSettings['xy'],self.oscSettings['xRange'],self.oscSettings['yRange'],self.oscSettings['buffSize'],self.score.tempo)
			#self.avSpace.startOSC(self.oscSettings['portIn'],self.oscSettings['ipAddrOut'],self.oscSettings['portOut'],self.oscSettings['xy'],self.oscSettings['xRange'],self.oscSettings['yRange'])
		else:
			self.oscDaemon.stopOSC()
			#self.avSpace.stopOSC()
		self.actionStart_OSC.setChecked(checked)

	
	def accStart(self,val):
		"""Changes the status of accelerometers analysis
		"""
		#print 'acc started'
		self.oscAcc = val
		self.oscDaemon.startAcc(val)
		# Change the status of the button!
		self.actionStart_Acc.setChecked(val)

	def playerStatus(self,playing):
		"""
		Check the status of the player and update the buttons status.
		@playing: boolean
		"""
		if playing: # check the button if not already checked
			if self.autoPlay: # If auto play
				if not self.actionReplay_Performance.isChecked():
					self.actionReplay_Performance.setChecked(True)
			else:	
				if not self.action_Play.isChecked():
					self.action_Play.setChecked(True)	
		else: # uncheck  the button if not already unchecked
			self.action_Play.setChecked(False)
			self.actionRecord.setChecked(False)
			self.actionReplay_Performance.setChecked(False)
			self.updateStatus("Player stopped",5000)
	
	def resetRules(self):
		"""
		Reset all rules to deadpan.
		"""
		self.activitySlider.setValueDouble(0.0)
		self.valenceSlider.setValueDouble(0.0)
		self.updateSliders(__K_DEADPAN__,__OVERALL_DEADPAN__)
	
	def updateRules(self):
		"""
			Update rule values vectors. Notice that these are common value with the activity-valence dialog, thus the need to lock-unlock.
		"""
		try:
			self.cmLock.lockForWrite()
			self.cmKVector[0] = self.rule1Spinbox.value()
			self.cmKVector[1] = self.rule2Spinbox.value()
			self.cmKVector[2] = self.rule3Spinbox.value()
			self.cmKVector[3] = self.rule4Spinbox.value()
			self.cmKVector[4] = self.rule5Spinbox.value()
			self.cmKVector[5] = self.rule6Spinbox.value()
			self.cmKVector[6] = self.rule7Spinbox.value()
			self.cmKVector[7] = self.rule8Spinbox.value()
			self.cmKVector[8] = self.rule9Spinbox.value()
			self.cmKVector[9] = self.rule10Spinbox.value()
			self.cmKVector[10] = self.rule11Spinbox.value()
			self.cmKVector[11] = self.rule12Spinbox.value()
			self.cmKVector[12] = self.rule13Spinbox.value()
			self.cmKVector[13] = self.rule14Spinbox.value()
			self.cmKVector[14] = self.rule15Spinbox.value()
			self.cmKVector[15] = self.rule16Spinbox.value()
			self.cmKVector[16] = self.rule17Spinbox.value()
			self.cmKVector[17] = self.rule18Spinbox.value()
			self.cmKVector[18] = self.rule19Spinbox.value()
		finally:
			self.cmLock.unlock()
		try:
			self.cmLock.lockForWrite()
			self.cmOverallScaling[0] = self.tempoSpinbox.value()
			self.cmOverallScaling[1] = self.dynamicsSpinbox.value()
		finally:
			self.cmLock.unlock()
			
	def updateSliders(self,k,overall):
		"""
		Refresh sliders when in playback mode or when the values are changed using activity-valence space.
		@k: vector (19 doubles)
		@overall: vector (2 doubles)
		"""
		self.rule1Spinbox.setValue(k[0])
		self.rule2Spinbox.setValue(k[1])
		self.rule3Spinbox.setValue(k[2])
		self.rule4Spinbox.setValue(k[3])
		self.rule5Spinbox.setValue(k[4])
		self.rule6Spinbox.setValue(k[5])
		self.rule7Spinbox.setValue(k[6])
		self.rule8Spinbox.setValue(k[7])
		self.rule9Spinbox.setValue(k[8])
		self.rule10Spinbox.setValue(k[9])
		self.rule11Spinbox.setValue(k[10])
		self.rule12Spinbox.setValue(k[11])
		self.rule13Spinbox.setValue(k[12])
		self.rule14Spinbox.setValue(k[13])
		self.rule15Spinbox.setValue(k[14])
		self.rule16Spinbox.setValue(k[15])
		self.rule17Spinbox.setValue(k[16])
		self.rule18Spinbox.setValue(k[17])
		self.rule19Spinbox.setValue(k[18])
		self.tempoSpinbox.setValue(overall[0])
		self.dynamicsSpinbox.setValue(overall[1])
		
	def updateSlidersDynamics(self,dyna):
		"""
		Updates dynamics slider
		"""
		self.dynamicsSpinbox.setValue(dyna)
	
	# This is used for the accelerometers control ...
	def updateTempoRatio(self,nPerSec):
		"""
		Updates the tempo slider by computing the ratio between the nominal tempo and the tempo sent as parameter, and updates the ACTIVITY
		"""
		#tempoRatio = 60*nPerSec/float(self.score.tempo)
		tempoRatio = nPerSec
		if tempoRatio>2:
			tempoRatio = 2
		elif tempoRatio<0.5:
			tempoRatio = 0.5
		
		self.tempoSpinbox.setValue(tempoRatio)
		self.updateActivityNoTempo((tempoRatio-0.5)/1.5) # Update activity
		
	
	# This is used for the accelerometers control ...
	def updateSlidersNoTempo(self,k,dyna):
		"""
		Refresh sliders when in playback mode or when the values are changed using activity-valence space. THIS ONE DOES NOT UPDATE TEMPO
		k: vector (19 doubles)
		@dyna (double)
		"""
		self.rule1Spinbox.setValue(k[0])
		self.rule2Spinbox.setValue(k[1])
		self.rule3Spinbox.setValue(k[2])
		self.rule4Spinbox.setValue(k[3])
		self.rule5Spinbox.setValue(k[4])
		self.rule6Spinbox.setValue(k[5])
		self.rule7Spinbox.setValue(k[6])
		self.rule8Spinbox.setValue(k[7])
		self.rule9Spinbox.setValue(k[8])
		self.rule10Spinbox.setValue(k[9])
		self.rule11Spinbox.setValue(k[10])
		self.rule12Spinbox.setValue(k[11])
		self.rule13Spinbox.setValue(k[12])
		self.rule14Spinbox.setValue(k[13])
		self.rule15Spinbox.setValue(k[14])
		self.rule16Spinbox.setValue(k[15])
		self.rule17Spinbox.setValue(k[16])
		self.rule18Spinbox.setValue(k[17])
		self.rule19Spinbox.setValue(k[18])
		self.dynamicsSpinbox.setValue(dyna)
	
	# This is used for the accelerometers control ...
	def updateActivityNoTempo(self,value):
		"""
		Refresh activity slider the value is changed using activity-valence space.
		@value: double
		THIS DOES NOT CHANGE TEMPO ALTHOUGH IT IS ACTUALLY COMPUTED
		"""
		coordinates = [value,self.valenceSlider.getValueDouble()]
		k,overall = self.score.av2kvec(coordinates)
		self.updateSlidersNoTempo(k,overall[1])
		#self.avSpace.setActivity(value) # Check the range...use it or not?
		#self.updateXY(coordinates + [1]) # This saves the performance in the file
		
	# This is used for the accelerometers control ...		
	def updateValenceNoTempo(self,value):
		"""
		Refresh valence slider the value is changed using activity-valence space.
		@value: double
		THIS DOES NOT CHANGE TEMPO ALTHOUGH IT IS ACTUALLY COMPUTED
		"""
		coordinates = [self.activitySlider.getValueDouble(),value]
		k,overall = self.score.av2kvec(coordinates)
		self.updateSlidersNoTempo(k,overall[1])
		#self.avSpace.setValence(value)
		#self.updateXY(coordinates + [1])
			
	def updateActivity(self,value):
		"""
		Refresh activity slider the value is changed using activity-valence space.
		@value: double
		"""
		coordinates = [value,self.valenceSlider.getValueDouble()]
		k,overall = self.score.av2kvec(coordinates)
		self.updateSliders(k,overall)
		self.avSpace.setActivity(value)
		self.updateXY(coordinates + [1])
		
	def updateValence(self,value):
		"""
		Refresh valence slider the value is changed using activity-valence space.
		@value: double
		"""
		coordinates = [self.activitySlider.getValueDouble(),value]
		k,overall = self.score.av2kvec(coordinates)
		self.updateSliders(k,overall)
		self.avSpace.setValence(value)
		self.updateXY(coordinates + [1])
		
	def updateXY(self,vect):
		"""
		Refresh X-Y coordinates when in playback mode.
		@vect: [x,y,flag]
		"""
		try:
			self.cmLock.lockForWrite()
			self.cmXY[0] = vect[0]
			self.cmXY[1] = vect[1]
			self.cmXY[2] = vect[2]
		finally:
			self.cmLock.unlock()
		
	def setXYFlag(self,on = 0):
		"""
		When recording a performance, this function sets the XY flag when the activity-valence space is used. It is needed because of the lock-unlock on the shared variable.
		@on = boolean (default = False)
		"""
		try:
			self.cmLock.lockForWrite()
			self.cmXY[2] = on
		finally:
			self.cmLock.unlock()
	
	def showAbout(self):
		"""
		Create and show a message box with information about the program.
		"""
		QtGui.QMessageBox.about(self, "About pyDM","""Interactive Performance Player v %s <p>Copyright &copy; 2009 Marco Fabiani All rights reserved. <p>This application can be used to interactively control the expressive performance of a midi (pDM) score. <p>Python %s - Qt %s - PyQt %s on %s""" % ( __version__, platform.python_version(), QtCore.QT_VERSION_STR, QtCore.PYQT_VERSION_STR, platform.system()))
		
	def showHelp(self):
		form = helpform.HelpForm("index.html",self)
		form.show()
		pass
		
	def updateBar(self,bar):
		"""
		Update the LCD counter for the current bar.
		@bar: int (current bar)
		"""
		self.barLcd.display(bar)
		self.barProgress.setValue(bar-1)

	def updateStatus(self, message, timeout = 5000):
		"""
		Change the message displayed in the status bar.
		@message: string
		@timeout: int (default 5000 ms, use 0 to show the message until a new one comes up)
		"""
		self.statusBar.showMessage(message, timeout)
		
	def closeFile(self):
		"""
		Closes the current open file, the performance player, and starts a new, empty player
		"""
		self.player.stop()
		self.player.terminate()
		self.player.wait()
		self.score.initialize()
		self.setScoreInfo()
		self.filenameOut = None
		self.filename = None
		self.fileIsOpen = False
	
	def updateFileMenu(self):
		"""
		This function overrides the creation of the file menu in ui_mainwindow_avsliders in order to add the recently opened files.
		"""
		self.menuRecentPerformances.clear()
		if self.filename is not None:
			current = QtCore.QString(self.filename)
		else:
			current = None
		recentFiles = []
		for fname in self.recentFiles:
			if fname != current and QtCore.QFile.exists(fname):
				recentFiles.append(fname)
		if recentFiles:
			for i, fname in enumerate(recentFiles):
				action = QtGui.QAction("&%d %s" % (i+1,QtCore.QFileInfo(fname).fileName()),self)
				action.setData(QtCore.QVariant(fname))
				self.connect(action,QtCore.SIGNAL("triggered()"),self.loadFile)
				self.menuRecentPerformances.addAction(action)
			
		
	def okToContinue(self):
		"""
		Check if we can close the file: if any changes were made, promt to save the file.
		"""
		"""if self.dirty:
			reply = QtGui.QMessageBox.question(self,"Unsaved Performance","Save unsaved changes?",QtGui.QMessageBox.Yes|QtGui.QMessageBox.No|QtGui.QMessageBox.Cancel)
			if reply == QtGui.QMessageBox.Cancel:
				return False
			elif reply == QtGui.QMessageBox.Yes:
				self.fileSave()"""
		return True		

	def closeEvent(self,event=None):
		"""
		Close the program: save settings, stop the player and destroy the midi player.
		"""
		# Save settings
		if self.okToContinue():
			settings = QtCore.QSettings()
			if self.filename is not None:
				filename = QtCore.QVariant(QtCore.QString(self.filename))
			else:
				filename = QtCore.QVariant()
			settings.setValue("LastFile",filename)
			
			if self.recentFiles:
				recentFiles = QtCore.QVariant(self.recentFiles)
			else:
				recentFiles = QtCore.QVariant()
			settings.setValue("RecentFiles",recentFiles)
			
			midisettings = QtCore.QString(pickle.dumps(self.midiSettings))
			oscsettings = QtCore.QString(pickle.dumps(self.oscSettings))
			settings.setValue("MidiSettings",QtCore.QVariant(midisettings))
			settings.setValue("OscSettings",QtCore.QVariant(oscsettings))
			
			settings.setValue("Geometry",QtCore.QVariant(self.saveGeometry()))
			settings.setValue("MainWindow/State",QtCore.QVariant(self.saveState()))		
			#Close the player
			self.player.stop()
			self.player.terminate()
			self.player.wait()
			self.midi.close()
			if self.actionStart_OSC.isChecked():
				try:
					self.oscDeamon.stopOSC()
				except:
					pass
		else:
			self.ignore()
		
	def addRecentFile(self,fname):
		"""
		Add recently opened files to the list.
		@fname: string
		"""
		self.filename = fname
		if fname is None:
			return
		if not self.recentFiles.contains(fname):
			self.recentFiles.prepend(QtCore.QString(fname))
			while self.recentFiles.count()>9:
				self.recentFiles.takeLast()
	
	def setScoreInfo(self):
		"""
		Prints the score information on the interface.
		"""
		self.titleValue.setText("<b>%s</>" % self.score.scoreName)
		self.tempoValue.setText("%s bpm" % self.score.tempo)
		self.meterValue.setText("%s" % self.score.meter)
		self.keyValue.setText("%s" % self.score.key)
		self.barProgress.setMaximum(self.score.bars)
		
	def playerClosed(self):
		"""
		Slot connected to the "terminated" signal sent out by the performance player
		"""
		pass
	
	def quitting(self):
		"""
		Close all active things before quitting
		"""
		self.oscDaemon.stopOSC()
		
	def selectFile(self,track):
		"""
		Select one of the 3 examples (hardcoded)
		"""
		self.oscStartStop(False)		
		if track == 1:
			fname = os.path.expanduser('~')+'/Agora/scores/Berio.pdm'
		elif track == 2:
			fname = os.path.expanduser('~')+'/Agora/scores/Joplin.pdm'
		elif track == 3:
			fname = os.path.expanduser('~')+'/Agora/scores/Mozart.pdm'
		else:
			pass
		self.scoreStop()
		if not self.okToContinue():
			return
		if self.filename is not None:
			dir = os.path.dirname(self.filename)
		else:
			dir = os.path.expanduser('~')
		formats = ["*.pdm"]
		#fname = unicode(QtGui.QFileDialog.getOpenFileName(self,"Choose a PDM file ...", dir, "Performance files (%s)" % " ".join(formats)))
#		if fname:
#			return self.loadFile(fname)
#		else: return False
		self.loadFile(fname)
		while True:
			try:
				self.oscStartStop(True)
				break
			except:
				pass
		

# Main function		
def main():
	app = QtGui.QApplication(sys.argv) 
	app.setOrganizationName("TMH-KTH") 
	app.setOrganizationDomain("speech.kth.se") 
	app.setApplicationName("pyDM") 
	app.setWindowIcon(QtGui.QIcon(":/icons/resources/pydm_icon.png")) 
	form = MainWindow() 
	form.show()
	app.connect(app,QtCore.SIGNAL("quit()"),form.quitting)
	sys.exit(app.exec_())
main()
