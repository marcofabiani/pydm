# This code has been significantly modified from the original source, which
# is distributed with Nokia's Python for Series 60, by Christopher Schmidt,
# and is released under the same license as that code (see below).
# 
# Copyright (c) 2008 Christopher Schmidt
# Copyright (c) 2005 Nokia Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
sys.path.append('E:\\python')

import appuifw
from graphics import *
import e32
import socket
import OSCmobile
import key_codes
from math import sqrt, pow

mobil=1
dampingfactor=1.4
tailsize=10
tailsampling=0.05

def colordamping(x):
   global dampingfactor
   red=(x & 0xff0000)/0x010000
   green=(x & 0x00ff00)/0x000100
   blue=x & 0x0000ff
   myred=int(round(red/dampingfactor))
   mygreen=int(round(green/dampingfactor))
   myblue=int(round(blue/dampingfactor))
   return myred*0x010000+mygreen*0x000100+myblue

# -- helper class
class struct(object):
   def __init__(self, *args, **kvargs):
	for key, val in kvargs.items():
	   setattr(self, key, val)
	   
# -- keypad mapping
keymap = {}
keymap[key_codes.EScancode1]	= struct(file=1)
keymap[key_codes.EScancode2]	= struct(file=2)
keymap[key_codes.EScancode3]	= struct(file=3)
keymap[key_codes.EScancode4]	= struct(stop=1)
keymap[key_codes.EScancode5]	= struct(play=1)
keymap[key_codes.EScancode6]	= struct(repeat=1)
keymap[key_codes.EScancode7]	= struct(rec=1)
keymap[key_codes.EScancode8]	= struct(playrec=1)
keymap[key_codes.EScancode9]	= struct(note=8)
keymap[key_codes.EScancodeStar]	= struct(acc=1)
keymap[key_codes.EScancode0]	= struct(note=10)
keymap[key_codes.EScancodeHash] = struct(note=11)
keymap[key_codes.EScancodeLeftArrow] = struct(rec=1)
keymap[key_codes.EScancodeRightArrow] = struct(playrec=1)
keymap[key_codes.EScancodeUpArrow] = struct(repeat=1)
keymap[key_codes.EScancodeDownArrow] = struct(stop=1)
keymap[key_codes.EScancodeSelect] = struct(play=1)

	      

######### WI-FI COMMUNICATION START
global HOST
global PORT
#HOST = '130.237.67.229' #roberto mac
#HOST = '130.237.67.116' #himork mac
#HOST = '169.254.219.122' #roberto-mb ad-hoc network
HOST = '255.255.255.255' #broadcast all
PORT = 9999 #the port in use



def choose_service(services):
	names = []
	channels = []
	for name, channel in services.items():
		names.append(name)
		channels.append(channel)
	index = appuifw.popup_menu(names, u"Choose service")
	return channels[index]


def sending2D(data1, data2, address):
#	HOST = '130.237.67.229' #roberto mac
#	HOST = '130.237.67.116' #himork mac
#	HOST = '169.254.219.122' #roberto-mb ad-hoc network
#	HOST = '255.255.255.255' #broadcast all
#	PORT = 9999 #the port in use
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	
	message = OSCmobile.OSCMessage()
	message.setAddress(address)
	message.append(data1)
	message.append(data2)	
	try:
		s.sendto((message.getBinary()), (HOST, PORT))
	except socket.error, err:
		pass
		
def sending3D(data1, data2, data3, mobile, address):
#	HOST = '130.237.67.229' #roberto mac
#	HOST = '130.237.67.116' #himork mac
#	HOST = '169.254.219.122' #roberto-mb ad-hoc network
#	HOST = '255.255.255.255' #broadcast all
#	PORT = 9999 #the port in use
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	
	message = OSCmobile.OSCMessage()
	message.setAddress(address)
	message.append(data1)
	message.append(data2)
	message.append(data3)
        message.append(mobile)
	try:
		s.sendto((message.getBinary()), (HOST, PORT))
	except socket.error, err:
		pass
		

def sendingControl(data1, data2, address):
#	HOST = '130.237.67.229' #roberto mac
#	HOST = '130.237.67.116' #himork mac
#	HOST = '169.254.219.122' #roberto-mb ad-hoc network
#	HOST = '255.255.255.255' #broadcast all
#	PORT = 9999 #the port in use
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	
	message = OSCmobile.OSCMessage()
	message.setAddress(address)
	message.append(data1)
	message.append(data2)
	try:
           s.sendto((message.getBinary()), (HOST, PORT))
	except socket.error, err:
		pass

def sendingWallnumber(data, address):
#	HOST = '130.237.67.229' #roberto mac
#	HOST = '130.237.67.116' #himork mac
#	HOST = '169.254.219.122' #roberto-mb ad-hoc network
#	HOST = '255.255.255.255' #broadcast all
#	PORT = 9999 #the port in use
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	
	message = OSCmobile.OSCMessage()
	message.setAddress(address)
	message.append(data)
	try:
		s.sendto((message.getBinary()), (HOST, PORT))
	except socket.error, err:
		pass


########## WI-FI COMMUNICATION END



########## Handle key pressed
def handleKey(key, state):
	global octave,channel,velocity
	global cutoff,detune
        global tailsize, locationList, location, blobcolorList, blobcolor, blobsizeList, blobsize, fileindex
	try:
		if (state == 1 and key == key_codes.EScancodeSelect):
			if (keymap[key].play == 1):
				sendingControl('play',1,"/pyDM/control")
#				sendingControl('acc', 0,"/pyDM/control")
				keymap[key].play = 0
			else:
				sendingControl('play', 0,"/pyDM/control")
#				sendingControl('acc', 1,"/pyDM/control")
				keymap[key].play = 1
		elif (state == 1 and key == key_codes.EScancodeDownArrow):
			sendingControl('stop',keymap[key].stop,"/pyDM/control")
			keymap[key_codes.EScancodeSelect].play = 1
			keymap[key_codes.EScancodeRightArrow].playrec = 1
#			sendingControl('acc', 1,"/pyDM/control")
		elif (state == 1 and key == key_codes.EScancodeUpArrow):
			if (keymap[key].repeat == 1):
				sendingControl('repeat',keymap[key].repeat,"/pyDM/control")
				keymap[key].repeat = 0
			else:
				sendingControl('repeat',keymap[key].repeat,"/pyDM/control")
				keymap[key].repeat = 1
		elif (state == 1 and key == key_codes.EScancodeLeftArrow):
			if (keymap[key].rec == 1):
				sendingControl('rec',keymap[key].rec,"/pyDM/control")
#				sendingControl('acc', 0,"/pyDM/control")
				keymap[key].rec = 0
			else:
				sendingControl('rec',keymap[key].rec,"/pyDM/control")
#				sendingControl('acc', 0,"/pyDM/control")
				keymap[key].rec = 1
		elif (state == 1 and key == key_codes.EScancodeRightArrow):
			if (keymap[key].playrec == 1):
				sendingControl('playrec',keymap[key].playrec,"/pyDM/control")
#				sendingControl('acc', 1,"/pyDM/control")
				keymap[key].playrec = 0
			else:
				sendingControl('playrec',keymap[key].playrec,"/pyDM/control")
#				sendingControl('acc', 0,"/pyDM/control")
				keymap[key].playrec = 1
		elif (state == 1 and key == key_codes.EScancodeStar):
			if (keymap[key].acc == 1):
				sendingControl('acc', 1,"/pyDM/control")
				keymap[key].acc = 0
                                locationList=[]
                                locationList.extend(location for i in xrange(0,tailsize))                                
                                blobcolorList=[]
                                blobcolorList.extend(blobcolor for i in xrange(0,tailsize))                                
                                blobsizeList=[]
                                blobsizeList.extend(blobsize for i in xrange(0,tailsize))
                        else:
				sendingControl('acc', 0,"/pyDM/control")
				keymap[key].acc = 1    
		elif (state == 1 and key == key_codes.EScancode1):
                   sendingControl('file', 1,"/pyDM/control")
                   keymap[key_codes.EScancodeSelect].play = 1
                   keymap[key_codes.EScancodeRightArrow].playrec = 1
                   fileindex=1
      		elif (state == 1 and key == key_codes.EScancode2):
                   sendingControl('file', 2,"/pyDM/control")
                   keymap[key_codes.EScancodeSelect].play = 1
                   keymap[key_codes.EScancodeRightArrow].playrec = 1
                   fileindex=2
      		elif (state == 1 and key == key_codes.EScancode3):
                   sendingControl('file', 3,"/pyDM/control")
                   keymap[key_codes.EScancodeSelect].play = 1
                   keymap[key_codes.EScancodeRightArrow].playrec = 1
                   fileindex=3

	except AttributeError:
	   if (state == 1):
	      try:
	   	controller = keymap[key].control	# controller number 0..127
	   	delta = keymap[key].delta		# amount to change
		value = 64				# just something to initialize
	   	if (controller == 74):
			cutoff = checkRange(cutoff + delta)
			value = cutoff
	   	elif (controller == 75):
			detune = checkRange(detune + delta)
			value = detune
		synth.MidiControl(0, controller, value)
		# onPaint()
	      except KeyError:
	        pass
	except KeyError:
	      pass

def onEvent(event):
	if event['type'] == appuifw.EEventKeyDown:  handleKey(event['scancode'], 1)
	elif event['type'] == appuifw.EEventKeyUp:  handleKey(event['scancode'], 0)

import sensor
class SensorConnection(object):
    delta = []
    def __init__(self):
        """Connect to the sensor."""
        sens = sensor.sensors()['AccSensor']
        self.s = sensor.Sensor(sens['id'], sens['category'])
        self.acceleration=()
        self.s.connect(self.callme)
    
    def callme(self, state):
       # compute x,y,z acceleration (g)
       self.acceleration = (-state['data_2']/340., -state['data_1']/340., state['data_3']/340.)
       #self.delta = []
       self.acceleration2 = []
       for key in ['data_1', 'data_2', 'data_3']:
          val = state[key]
          self.acceleration2.append(val/680.)
          #self.delta.append(int(val + 40)/80)

    def cleanup(self):
        """Cleanup after yourself. *Must be called* before exiting."""
        self.s.disconnect()

sense_conn = SensorConnection()


appuifw.app.screen='full'
img=None
def handle_redraw(rect):
   if img:
      canvas.blit(img)
# appuifw.app.body=canvas=appuifw.Canvas(
#     redraw_callback=handle_redraw)
appuifw.app.body=canvas=appuifw.Canvas(event_callback=onEvent, redraw_callback=handle_redraw)
img=Image.new(canvas.size)
img2=Image.open("e:\\kth_logo_f2.gif")

running=1
def quit():
    global running
    running=0

appuifw.app.exit_key_handler=quit

location=[img.size[0]/2,img.size[1]/2]
locationList=[]
locationList.extend(location for i in xrange(0,tailsize))

sending2D(location[0]/img.size[0]*2-1, location[1]/img.size[1]*2-1, "/pyDM/xy")
speed=[0.,0.]
blobsize=16
blobsizeList=[]
blobsizeList.extend(blobsize for i in xrange(0,tailsize))
blobcolorList=[0x00ff00]
blobcolorList.extend(0 for i in xrange(0,tailsize-1))

xs,ys=img.size[0],img.size[1]
acceleration=0.05 #original value
#friction = 0.993 #original value
friction = 0.890

#acceleration=0.5
#friction = 0.9999

import time
start_time=time.time()
nexttime=start_time+tailsampling
n_frames=0
fileindex=1

labeltext=[u'Tilt the phone to move', u'Berio - Wasserklavier', u'Joplin - Maple leaf rag', u'Mozart - KV1e']
textlengths=map(len,labeltext)
textrect=img.measure_text(labeltext[textlengths.index(max(textlengths))], font='normal')[0]
text_img=Image.new((textrect[2]-textrect[0],textrect[3]-textrect[1]))

while running:
    text_img.clear(0)
    text_img.text((-textrect[0],-textrect[1]),labeltext[fileindex],fill=0xffffff,font='normal')
    img.clear(0)
    img.blit(text_img, (0,0))
    img.blit(img2, (-xs/2,-ys/2))
    if keymap[key_codes.EScancodeStar].acc == 1:
       for i in xrange(tailsize-1,-1,-1):
          img.point((locationList[i][0]+blobsizeList[i]/2, locationList[i][1]+blobsizeList[i]/2),  blobcolorList[i],  width=blobsizeList[i])
    else:
       img.point((location[0]+blobsize/2, location[1]+blobsize/2), blobcolor, width=blobsize)
    
    handle_redraw(())
    e32.ao_yield()
    e32.reset_inactivity()
    speed[0]*=friction
    speed[1]*=friction
    location[0]+=speed[0]
    location[1]+=speed[1]
    sending2D(2*(location[0]+blobsize/2)/xs-1, 2*(location[1]+blobsize/2)/ys-1, "/pyDM/xy") #sending data for 2D control of activity valence using 2D acceleration: "balancing the performance"
    sending3D(sense_conn.acceleration2[0], sense_conn.acceleration2[1], sense_conn.acceleration2[2], mobil, "/pyDM/accraw") #sending data for controlling using gestures: 3D acceleration
    n_frames+=1
    
    if not sense_conn: continue

    #if not len(sense_conn.delta): continue
    if not len(sense_conn.acceleration): continue
    if (keymap[key_codes.EScancodeStar].acc == 1):
    	acceleration=0.05 #original value
    	friction = 0.993 #original value

    else:
    	acceleration=0.4
    	friction = 0.9999
        #friction = 1



    if location[0] >= xs-blobsize/2:
    	sendingWallnumber(1,"/pyDM/acc") # The ball hits wall #1
        #location[0]=2*xs-location[0]-blobsize
        location[0]=xs-blobsize/2
        speed[0]= -x_bounce_factor * speed[0]
        speed[1]=0.90*speed[1]
    if location[0] <= -blobsize/2:
    	sendingWallnumber(2,"/pyDM/acc") # The ball hits wall #2
        #location[0]=-location[0]-blobsize
        location[0]=-blobsize/2
        speed[0]= -x_bounce_factor * speed[0]
        speed[1]=0.90*speed[1]
    if location[1] >= ys-blobsize/2:
    	sendingWallnumber(3,"/pyDM/acc") # The ball hits wall #3    
        #location[1]=2*ys-blobsize-location[1]
        location[1]=ys-blobsize/2
        speed[0]=0.90*speed[0]
        speed[1]= -y_bounce_factor * speed[1]
    if location[1] <= -blobsize/2:
    	sendingWallnumber(4,"/pyDM/acc") # The ball hits wall #4
        #location[1]=-location[1]-blobsize
        location[1]=-blobsize/2
        speed[0]=0.90*speed[0]
        speed[1]= -y_bounce_factor * speed[1]



    #if location[0] < xs/2:
    #   if location[1] < ys/2:
    ##      blobcolor=0xff00ff
    #  else:
    #      blobcolor=0x0000ff
    #else:
    #   if location[1] < ys/2:
    #      blobcolor=0xffff00
    #   else:
    #      blobcolor=0xff0000

    if (keymap[key_codes.EScancodeStar].acc == 1):
       x_bounce_factor = .6 * (1 - min(6,abs(sense_conn.acceleration[0])) / 9) 
       y_bounce_factor = .6 * (1 - min(6,abs(sense_conn.acceleration[1])) / 9)
       
       redvalue1=min(0xff, max(0, int(round(0xff*(1-(location[1]+blobsize/2)/ys)))))
       redvalue2=min(0xff, max(0,int(round((0xff-redvalue1)*(location[0]+blobsize/2)/xs+redvalue1))))
       greenvalue1=min(0xff, max(0, int(round(0xff*(1-(location[1]+blobsize/2)/ys)))))
       greenvalue2=min(0xff,max(0,int(round(greenvalue1*(location[0]+blobsize/2)/xs))))
       bluevalue=min(0xff,max(0,int(round(0xff*(1-(location[0]+blobsize/2)/xs)))))
    else:
       x_bounce_factor = .8 * (1 - min(6,abs(sense_conn.acceleration[0])) / 9) 
       y_bounce_factor = .8 * (1 - min(6,abs(sense_conn.acceleration[1])) / 9)
       
       velocityratio = pow(pow(speed[0],2) + pow(speed[1],2), 1/3.)/pow((xs*xs+ys*ys)/4, 1/3.)
       redvalue2 = int(round(0xff*velocityratio))
       greenvalue2=0
       bluevalue=int(round(0xff*(1-velocityratio)))

    blobcolor=0x010000*redvalue2+0x000100*greenvalue2+bluevalue


    blobsize=16+int(round(sqrt(pow(location[0]-xs/2.,2)+pow(location[1]-ys/2.,2))/8))+(1-keymap[key_codes.EScancodeStar].acc)*64


    ax,ay,az=sense_conn.acceleration
    # nonlinear mapping (acc^2)
    speed[0] += (cmp(ax,0)*ax*ax) * acceleration * min(xs,ys) * friction
    speed[1] += (cmp(ay,0)*ay*ay) * acceleration * min(xs,ys) * friction

    
   # speed[0] -= (sense_conn.delta[1]) * acceleration
   # speed[1] -= (sense_conn.delta[0]) * acceleration

    speed[0] = max(min(xs / 2, speed[0]), -xs/2)
    speed[1] = max(min(ys / 2, speed[1]), -ys/2)
    

    if (keymap[key_codes.EScancodeStar].acc == 1) and  (time.time() > nexttime):
       nexttime+=tailsampling
       nothing1 = blobcolorList.pop()
       blobcolorList=map(colordamping, blobcolorList)
       blobcolorList.insert(0,blobcolor)
       nothing2 = locationList.pop()
       locationList.insert(0, [location[0],location[1]])
       nothing3 = blobsizeList.pop()
       blobsizeList.insert(0,blobsize)



end_time=time.clock()
total=end_time-start_time

sense_conn.cleanup()

#del s

#print "%d frames, %f seconds, %f FPS, %f ms/frame."%(n_frames,total,
#                                                     n_frames/total,
#                                                     total/n_frames*1000.)
                                                     
