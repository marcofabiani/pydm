#!usr/bin/env/ python
# pyDM : python pdm player and performance control
# Copyright (c) Marco Fabiani 2008
#
# controlspace.py - Generic class that creates a x-y space (activity valence for example)
#
# Changes:
# 2009-06-04: Added thread to update the queue FIXME! Could it be left open???
# 2008-10-30: Changed the colors from quadrants to circle on black background


import sys,time,Queue,threading,math
from PyQt4 import QtCore, QtGui

global X_MAX
global Y_MAX
global LOGICALSIZE
LOGICALSIZE = 1000
X_MAX=400
Y_MAX=400
TAILLEN = 30

class paintThread (threading.Thread):

	def __init__ ( self, cSpace):
		self.cSpace = cSpace
		threading.Thread.__init__ ( self )
		self.on = 1
		
	def run (self):
		while self.on:
			time.sleep(0.1)
			self.cSpace.update()
			
	def stop(self):
		self.on = 0
		
class controlSpace(QtGui.QWidget):
    #def __init__(self, xLabel='xAxis' , yLabel = 'yAxis', parent = None):
    def __init__(self, xLabel='xAxis' , yLabel = 'yAxis',ruLabel = 'RU',luLabel = 'LU',rdLabel = 'RD',ldLabel = 'LD', parent = None):
        """
        Initialize the control space.
        @xLabel: string (default 'xAxis')
        @yLabel: string (default 'yAxis')
        """
        
        super(controlSpace,self).__init__(parent)
        self.xLabel = QtGui.QLabel(self)
        self.yLabel = QtGui.QLabel(self)
        self.ruLabel = QtGui.QLabel(self)
        self.luLabel = QtGui.QLabel(self)
        self.ldLabel = QtGui.QLabel(self)
        self.rdLabel = QtGui.QLabel(self)

        self.xLabel.setText('<font color= white size = 5>'+ xLabel + '</font>')
        self.yLabel.setText('<font color= white size = 5>'+ yLabel + '</font>')
        self.ruLabel.setText('<font color= white size = 5>'+ ruLabel + '</font>')
        self.luLabel.setText('<font color= white size = 5>'+ luLabel + '</font>')
        self.ldLabel.setText('<font color= white size = 5>'+ ldLabel + '</font>')
        self.rdLabel.setText('<font color= white size = 5>'+ rdLabel + '</font>')
        
        
        self.setMinimumHeight(X_MAX)
        self.setMinimumWidth(Y_MAX)
        self.focalPoint = QtCore.QPointF(0,0)
        #self.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.tailQueue = Queue.Queue()
        self.color1Off = QtGui.QColor()
        self.color1Off.setHsvF(0.86,0.3,1)
        self.color2Off = QtGui.QColor()
        self.color2Off.setHsvF(0.17,0.3,1)
        self.color3Off = QtGui.QColor()
        self.color3Off.setHsvF(0,0.3,1)
        self.color4Off = QtGui.QColor()
        self.color4Off.setHsvF(0.66,0.3,1)
        self.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding,QtGui.QSizePolicy.MinimumExpanding))
        self.setMouseTracking(1)
        
        # Start a thread that updates the interfaces every x ms, so that the queue, if not moved, collapses in a singles point
        self.repeatPaint = paintThread(self).start()
        		
        
    def mousePressEvent(self, event): # Bug????
        """
        Move the cursor to the correct point when left button is pressed.(doesn't work...)
        """
        if (event.button()==QtCore.Qt.LeftButton):
            self.updatePosition(event.pos())
   		
    def mouseMoveEvent(self, event):
        """
    	When the left button is down, track mouse position.
    	"""
        if (event.buttons() & QtCore.Qt.LeftButton):
        	self.updatePosition(event.pos())
			
    def updatePosition(self,pos):
        """
    	Update the position on the control space and send out a message with the new coordinates
    	"""
        # Check for out of bounds
        if (pos.x()<self.width()) and (pos.x()>0):
        	xLogical = LOGICALSIZE*(pos.x()-self.width()/2)/self.width()
        elif (pos.x()<0):
        	xLogical = -LOGICALSIZE/2
        else:
        	xLogical = 	LOGICALSIZE/2
        self.focalPoint.setX(xLogical)
        self.emit(QtCore.SIGNAL("xChanged(double)"),2*float(xLogical)/LOGICALSIZE)
        	
        if (pos.y()<self.height()) and (pos.y()>0):
        	yLogical = -LOGICALSIZE*(pos.y()-self.height()/2)/self.height()
        elif (pos.y()<0):
        	yLogical = LOGICALSIZE/2
        else:
        	yLogical = -LOGICALSIZE/2
        self.emit(QtCore.SIGNAL("yChanged(double)"),2*float(yLogical)/LOGICALSIZE)
        self.focalPoint.setY(yLogical)
        self.update()
    
    def paintEvent(self,event=None):
    	"""
    	Paint event: update the circles
    	"""
    	y = self.focalPoint.y()/(LOGICALSIZE/2)
    	x = self.focalPoint.x()/(LOGICALSIZE/2)
    	side = min(self.width(), self.height())
    	painter = QtGui.QPainter(self)
    	painter.setWindow(-LOGICALSIZE/2,-LOGICALSIZE/2,LOGICALSIZE,LOGICALSIZE)
    	painter.setViewport((self.width() - side)/2,(self.height() - side)/2,side,side)
    	# Draw backgournd
    	color = QtCore.Qt.black
    	painter.setBrush(QtGui.QBrush(color))
    	painter.drawRect(-LOGICALSIZE/2,-LOGICALSIZE/2,LOGICALSIZE,LOGICALSIZE)
    	

    	#painter.drawEllipse(QtCore.QPointF(self.focalPoint.x(),-self.focalPoint.y()),50,50)
    	
    	# Save the main ball position in the queue
    	
    	# Draw the balls in the queue
    	temp = Queue.Queue(TAILLEN)
    	color = QtGui.QColor()
    	k = 1
    	while not self.tailQueue.empty():
    		# Draw main ball
    		#color = QtCore.Qt.white
    		point = self.tailQueue.get()
    		x = point.x()/LOGICALSIZE*2
    		y = -point.y()/LOGICALSIZE*2
    		if (x>=0 and y>=0):
    			h = 60.0 # Yellow
    			v = 1
    		elif (x>0 and y<0):
    			h = 0.0 # Red
    			v = 1
    		elif (x<0 and y>0):
    			h = 300.0 # Magenta
    			v = 1
    		else:
    			h = 240.0 # Blue
    			v = 0.5
    			
    		h = h/360.0
    		# Compress scale
    		#s = -abs(math.pow((abs(x)-1),3))+1
    		#v = -abs(math.pow((abs(y)-1),3))+1
    		#s = -abs(math.pow((abs(y)-1),3))+1
    		#s = math.sqrt((x*x+y*y)/2)
    		s = (3*((2 - (y + 1)) / 2)/4)+0.25
    		#print s
    		
    		color.setHsvF(h,s,v)
    		color.setAlphaF(float(k)/TAILLEN)
    		painter.setBrush(color)
    		painter.setPen(color)
    		#radius = (3*k)*(math.sqrt(x*x+y*y)+0.3)
    		#radius = (3*k)*(abs(x)+0.3)
    		radius = 40*k/TAILLEN*(x+1+0.3)
    		painter.drawEllipse(point,radius,radius)
    		temp.put(point)
    		k= k+1
    		
    	if temp.full():
    		trash = temp.get()
    	
    	temp.put(QtCore.QPointF(self.focalPoint.x(),-self.focalPoint.y()))
    	
    	while not temp.empty():
    		point = temp.get()
    		self.tailQueue.put(point)
    	
    def paintTail(self,event = None):
    	"""
    	Updates the transparency of the tail and removes the circles from the queue when the transparency is zero
    	"""
    	pass
    	# Draw tail
    	# 1) check if it is time to update it using a timer: every 10ms reduce
    	
    	# 2) draw the balls from the positions vector

    def resizeEvent(self, event):
    	"""
    	Resize window (override function)
    	"""
    	side = min(self.width(), self.height())
    	self.resize(side,side)
    	
    	# Y label
    	x = self.width()- self.yLabel.width() - self.yLabel.height()
    	y = self.yLabel.height()
    	self.yLabel.move(x,y)
    	
    	# X label
    	x = self.width() - self.xLabel.width()-self.xLabel.height()
    	y = (self.height()-self.xLabel.height())/2
    	self.xLabel.move(x,y)
    
    	# RU label
    	x = self.width() - self.ruLabel.width()-self.ruLabel.height()
    	y = self.ruLabel.height()
    	self.ruLabel.move(x,y)
    	
    	# LU label
    	x = self.luLabel.height()
    	y = self.luLabel.height()
    	self.luLabel.move(x,y)
    	
    	# RD
    	x = self.width() - self.rdLabel.width()-self.rdLabel.height()
    	y = self.height()-2*self.rdLabel.height()
    	self.rdLabel.move(x,y)
    	
    	# LD
    	x = self.ldLabel.width()
    	y = self.height()-2*self.ldLabel.height()
    	self.ldLabel.move(x,y)
    	
    def setX(self,x):
    	"""
    	Set @x coordinate
    	"""
    	if (self.focalPoint.x() != x*LOGICALSIZE/2) :
    		self.focalPoint.setX(x*LOGICALSIZE/2)
    		self.emit(QtCore.SIGNAL("xChanged(double)"),x)
    	self.update()
    	
    def setY(self,y): 
    	"""
    	Set @y coordinate
    	NOTICE THE minus sign!
    	"""
    	if (self.focalPoint.y() != y*LOGICALSIZE/2):
    		self.focalPoint.setY(y*LOGICALSIZE/2)
    		self.emit(QtCore.SIGNAL("yChanged(double)"),y)
    	self.update()
    
    def reset(self):
    	"""
    	Clear the tail
    	"""
    	while not self.tailQueue.empty():
    		trash = self.tailQueue.get()
    	self.setX(0)
    	self.setY(0)
    	
    def close(self):
    	"""
    	Stops the thread
    	"""
    	self.repeatPaint.stop()
    	self.repeatPaint.join()

		
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = controlSpace('','')
    window.show()
    sys.exit(app.exec_())