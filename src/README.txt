README.TXT
This is a version where the notes are managed by a single thread.

##############
How to create a self-contained OS X app:
- install py2app
- in the terminal run: $ python setup_mac.py py2app

##############
How to create a self-contained Windows app:
- install py2exe
- in the terminal run: $ python setup_win.py py2exe --includes sip