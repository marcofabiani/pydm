#!usr/bin/env/ python
# pdmConvert : convert a pdm score into other formats (GMN format (Guido Music Notation), MIDI, musicXML)
# Copyright (c) Marco Fabiani 2009
#

import math

__NOTES__ = ["c" , "c#" , "d" , "d#" , "e" , "f" , "f#" , "g" , "g#" , "a" , "a#" , "b"]

def pdm2gmn(pdmScore,quantize = 32):
	"""
	Convert a pdm score into a gmn score.
	@pdmScore: file object type (use pdmScore = open(filename))
	@quantize: shorter note to be represented (default = 1/16)
	Returns gmnScore: string
	"""
	# TODO: breaks, chords, tracks
	
	scoreList = []
	openB = 0
	for line in pdmScore:
		line = line.split(";")[0]
		tmp = line.split(" ")
		delta = int(tmp[0])
		command = tmp[1]
		parameters = tmp[2:]
		#if delta>0:
		#	if openB:
		#		scoreList.append("} ")
		#		openB = 0
		#	scoreList.append("{ ")
		#	openB = 1
		if command == "METER":
			scoreList.append("\\meter<\""+parameters[0]+"/"+parameters[1]+"\"> ")
		elif command == "TEMPO":
			# Needed here to compute the duration of quarter notes in ms
			qrtDur = round(float(parameters[0])/1E3)
		elif command == "BAR": # Maybe better to only insert the first bar and leave the rest automatic?
			scoreList.append("\\bar ")			
		elif command == "NOTE": # Chords? Need to consider DTs
			noteName = midiNum2gmnStr(int(parameters[0]))
			
			#Quantize the duration
			qPow = log2(quantize)
			dur = int(parameters[3])
			minDur = qrtDur*4/quantize # FIXME int/float/round?
			nQuant = int(round((dur/float(minDur)))) #number of Quantums
			fracDur = mcd(nQuant,quantize)
			
			if fracDur[0] == 1:
				scoreList.append(noteName + "/" + str(fracDur[1])+ " ")
			else: # need to decompose the fraction!
				decomposition = decompFrac(fracDur)
				scoreList.append("\\tieBegin ")
				for i in range(len(decomposition)):
					if decomposition[i]:
						scoreList.append(noteName + "/" + str(pow(2,i))+ " ")
				scoreList.append("\\tieEnd ")
	# Join the string
	if openB:
		scoreList.append("} ")
	return ''.join(scoreList)

	
def midiNum2gmnStr(note):
	"""
	Convert a midi note number to a string, using the GMN standard (60 = C4 = c1)
	"""
	# Find the note name
	return __NOTES__[int(note - 12*(int(note)/12))]+str(int(note)/12 - 4)
	
def log2(n):
	"""
	logarithm base 2
	"""
	return math.log(n)/math.log(2)
	
def mcd(num,den):
	"""
	reduce to the lowest den power of 2
	"""
	while (not den%2) and (not num%2): # reduce to mcd
		den = den/2
		num = num/2
	return [num , den]
	
def decompFrac(frac):
	"""
	Decomposes the fraction in single parts
	"""
	decomposition = [0, 0, 0, 0, 0, 0]
	div = frac[1]/2
	rem = frac[0]%div
	#print rem,div
	tmp = mcd(div*(frac[0]/div),frac[1])
	#print tmp
	if tmp[0]>0:
		decomposition[int(log2(tmp[1]))] = tmp[0]
	while rem > 1:
		div = div/2
		remOld = rem
		rem = rem%div
		tmp = mcd(div*(remOld/div),frac[1])
	#	print div,remOld, rem , tmp
		decomposition[int(log2(tmp[1]))] = tmp[0]
	if rem == 1:
		decomposition[int(log2(frac[1]))] = 1
	return decomposition
	
	