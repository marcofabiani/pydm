#!usr/bin/env/ python
# pyDM : python pdm player and performance control
# Copyright (c) Marco Fabiani 2008
#
# perfplayer.py
# Generic class to handle read-write and interpretation of score files. This class does not
# depend on any GUI framework.

import os , sys, string, time
import midiplayer

__K_DEADPAN__ = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
__OVERALL_DEADPAN__ = [1,0]

# Fore activityvalence to K conversion
__HAPPY_CORNER_K__ = [0,1,1,0,0,0,0,0.5,1.8,0,0,0,1.5,0,0,2,0,0,2.5]
__HAPPY_CORNER_OVERALL__ = [1.1,3]
__TENDER_CORNER_K__ = [0,1.5,1.5,0,0,0,0,0.5,1.2,0,0,0,0,0,0,1,0,0,0.7]
__TENDER_CORNER_OVERALL__ = [0.8,-4]
__SAD_CORNER_K__ = [0,3,3,0,0,0,0,0.5,1,0,0,0,-2,0,0,0.8,0,0,0]
__SAD_CORNER_OVERALL__ = [0.6,-7]
__ANGRY_CORNER_K__ = [0,-1,-1,0,0,0,0,0,1.4,0,0,0,2.5,0,0,1.5,0,0,1]
__ANGRY_CORNER_OVERALL__ = [1.2,7]


# Synth default curves
__ROLANDJV1010__ = [63.883,4.0845,0.1193,0.0017]
__SOUNDBLASTER__ = [66.603,3.2466,0.1008,0.0024]
#__DISKLAVIER__ = [94.899,5.0055,0.0112,-0.0004]
__DISKLAVIER__ = [73.5,5.35,0.0,0.0]

class PerfPlayer:
	def __init__(self,midiPlayer):
		"""
		Creates a performance player that reads/saves score files and is able to interpret each line and in the score and perform the correct action.
		@midiPlayer: midiplayer.midiPlayer() -> platform dependent. see midiplayer.py this manages low-level midi playback.
		"""
		
		self.player = midiPlayer
		self.scoreFile = []

		self.currentPosition = 0
		self.offList = []

		self.headerStart = 0
		self.trackStart = 0
		self.muted = True
		self.fileType = "-"
		self.scoreName = None
		self.meter = "-"
		self.tempo = "-"
		self.key = "-"
		self.bars = 1
		self.recording = False
		self.fileType = None
		
		self.trackProgram = [1,1]
		self.trackVolume = [0,1]
		self.kVector = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		self.overallPar = [1,0]
		self.flagXY = [0,0,0]
		self.DT = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		self.DSL = [0,0,0,0,0,0,0,0,0,0,0]
		self.DART = [0,0,0,0,0]
		
#		self.synth = __ROLANDJV1010__
#		self.synth = __SOUNDBLASTER__
		self.synth = __DISKLAVIER__
		
		# set up a dictionary of actions for the commands

		self.__takeaction = {
   			"FILETYPE": self.__doFileType,
   			"VERSION": self.__doVersion,
   			"HEADERSTART": self.__doHeaderStart,
   			"SCORENAME": self.__doScoreName,
   			"METER": self.__doMeter,
   			"TEMPO": self.__doTempo,
   			"KEY": self.__doKey,
   			"TRACKNAME": self.__doTrackName,
   			"SYNTH": self.__doSynth,
   			"PROGRAM": self.__doProgram,
   			"VOL": self.__doVol,
   			"TRACKDELAY": self.__doTrackDelay,
   			"HEADEREND": self.__doHeaderEnd,
   			"BAR": self.__doBar,
   			"DT": self.__doDT,
   			"DSL": self.__doDSL,
   			"DART": self.__doDART,
   			"NOTE": self.__doNote,
   			"KVEC": self.__doKVec,
   			"KOVERALL": self.__doKOverall,
   			"XY": self.__doXY,
   			"SUSPED": self.__doSusPed,
   			"ONESTRPED": self.__doOneStrPed
   			}
		
	def initialize(self):
		"""
		Re-initializes the peformance to the default status.
		"""
		self.scoreFile = []
		self.currentPosition = 0
		self.headerStart = 0
		self.trackStart = 0
		self.muted = False
		self.fileType = "-"
		self.scoreName = None
		self.meter = "-"
		self.tempo = "-"
		self.key = "-"
		self.bars = 1
		self.currentBar = 1
		self.trackProgram = [1,1]
		self.trackVolume = [0,1]
		self.kVector = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		#self.kVectorAuto = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		self.overallPar = [1,0]
		#self.overallParAuto = [1,0]
		self.DT = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		self.DSL = [0,0,0,0,0,0,0,0,0,0,0]
		self.DART = [0,0,0,0,0]

	def openPDMScore(self,filename):
		"""
		Opens and sorts a pdm score file.
		@filename: string
		"""
		try:
			f = open(filename,'r') # open file in read-only
			# read the entire file in memory
			self.scoreFile = f.readlines()
			f.close()
		except IOError, (errno, strerror):
			return False
		# remove semicolon and return character
		n=0
		for line in self.scoreFile:
			self.scoreFile[n] = line.split(";")[0]
			n += 1	
		# Set all the values in the header
		self.currentPosition = 0
		self.bars = 1
		result = None
		while result!="headerend":
			result = self.__doCommandFast(self.scoreFile[self.currentPosition])
			self.currentPosition += 1
		# If not already there	
		# Insert the space for the perfomance recording and find the number of bars
		n = len(self.scoreFile)-1
		while n>self.trackStart:
			result = self.__doCommandFast(self.scoreFile[n])
			if (result == "note" and self.fileType == 'pDM_score_file'):
				self.scoreFile.insert(n,'0 XY 0 0 0') # X,Y and flag: if 1, use the XY during playback			
				self.scoreFile.insert(n,'0 KVEC 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0')
				self.scoreFile.insert(n,'0 KOVERALL 1 0')
			elif (result == "bar") and (self.currentBar[0] > self.bars):
				self.bars = self.currentBar[0]
			else: pass
			n = n-1
		self.scoreFile[0] = "0 FILETYPE pDM_performance_file"
		self.rewindToStart()					
		return True
	
	def openXMLScore(self,filename): 
		"""
		Open an XML performance file (to be defined)
		@filename: string
		"""
		pass
		
	def savePerformancePDM(self,filename):
		"""
		Save the performance as a PDM file. The difference here with DM are the additional lines "XY", "KVEC" and "KOVERALL" where the performance is stored along with the score. The file type is "pDM_performance_file"
		@filename: string
		"""
		f = open(filename,'w')
		for line in self.scoreFile:
			f.write(line+";\n")
		f.close()		
	
	def rewindToStart(self):
		"""
		Rewind to the first note (end of file header)
		"""
		self.jumpTo(self.trackStart)
		self.currentBar = [1, self.currentPosition]
	
	def jumpTo(self,line):
		"""
		Jump to @line in the file
		"""
		self.currentPosition = line
	
	def jumpToBar(self,step):
		"""
		Jump @step number of bars. Negative @step jump back.
		"""
		targetBar = self.currentBar[0] + step
		if step>0:
			direction = 1
		else: direction = -1
		# check for end or begining of track
		while (not self.endFile()) and (self.currentPosition >= self.trackStart):
			#print self.currentPosition
			self.__doCommandFast(self.scoreFile[self.currentPosition])
			if self.currentBar[0] == targetBar:
				break
			self.currentPosition = self.currentPosition + direction
		self.currentPosition = self.currentBar[1]
		
	def setRecording(self,rec = False):
		"""
		Set recording flag.
		@rec: boolean (default = False)
		"""
		self.recording = rec
	
	def updateKVec(self,ks):
		"""
		Update current perfomance "point" 
		@ks: vector of doubles (19)
		"""
		self.kVector = ks
		
	def updateOverall(self,parms):
		"""
		Update current perfomance "point"
		@parms: vector of doubles (2)
		"""
		self.overallPar = parms
		
	def updateXY(self,XYF):
		"""
		Update current perfomance "point" in activity-valence space
		@XYF: [x,y,flag]
		"""		
		self.flagXY = XYF
		
	def getKVec(self):
		"""
		Return current K vector.
		"""
		return self.kVector
		
	def getOverall(self):
		"""
		Return current overall vector (tempo,sl)
		"""
		return self.overallPar
	
	def getXY(self):
		"""
		Return activity-valence coordinates and flag
		"""
		return self.flagXY
	
	def readLine(self):
		"""
		Read a line from the file and send the command to the interpreter.
		"""
		self.__doCommand(self.scoreFile[self.currentPosition])
	
	def incPos(self):
		"""
		Increment current position.
		"""
		self.currentPosition += 1
		
	def decPos(self):
		"""
		Decrement current position
		"""
		self.currentPosition -= 1 
		
	def endFile(self):
		"""
		Check if the end of file has been reached
		"""
		return self.currentPosition >= len(self.scoreFile)
		
	def getBar(self):
		"""
		Return the current bar.
		"""
		return self.currentBar[0]
		
	def allOff(self,wait):
		"""
		Stop all notes still playing. If wait is TRUE, wait for the notes to end "naturally", otherwise kill them immediatly
		"""
		if wait:
			while not self.offList == []:
				for off in self.offList[:]:
					if int(off[2]) < self.player.getMidiTime():
						self.player.noteOff(off[1],off[0])
						self.offList.remove(off)
				time.sleep(0.001)
		else:
			for off in self.offList[:]:
				self.player.noteOff(off[1],off[0])
				self.offList.remove(off)
		try:
			self.player.sustainPedal(0)
			self.player.softPedal(0)
		except:
			pass
		
#######################################################################################
	# Command interpreter: functions to be run at different commands read from the score file
	def __doCommand(self,line): 
		"""
		Decomposes the command line and controls the timing of the commands
		"""
		# split the command line
		self.muted = False
		tmp = line.split(" ")
		task = tmp[1]
		parameters = [tmp[2:]]
		deltaT = int(tmp[0])
		# update the current time if next event is delayed
		if deltaT > 0:
			nextEvent = self.player.getMidiTime() + deltaT/(self.__dtModify())
			# g to process the event
			while self.player.getMidiTime() < nextEvent:
				time.sleep(.001)
				# check if any note off message is due
				for off in self.offList[:]:
					if int(off[2]) < self.player.getMidiTime():
						self.player.noteOff(off[1],off[0])
						self.offList.remove(off)
		return self.__takeaction.get(task,self.__errhandler)(*parameters)
		
	def __doCommandFast(self,line): 
		"""
		Like doCommand, but without timing control
		"""
		# split the command line
		self.muted = True
		tmp = line.split(" ")
		task = tmp[1]
		parameters = [tmp[2:]]
		tmp[0] = int(tmp[0])
		# update the current time if next event is delayed
		return self.__takeaction.get(task,self.__errhandler)(*parameters)		
		
	def __doFileType (self,parList):
		"""
		Read the file type
		"""
		self.fileType = parList[0]

	def __doVersion (self,parList):
		"""
		Read file version.
		"""
		pass 
	
	def __doHeaderStart (self,parList):
		"""
		Save header start position.
		"""
		self.headerStart = self.currentPosition
		
	def __doScoreName (self,parList):
		"""
		Save score name
		"""
		self.scoreName = parList[0]
	
	def __doMeter (self,parList):
		"""
		Save meter
		"""
		self.meter = "%d/%d" % (int(parList[0]),int(parList[1]))
		
	def __doTempo (self,parList):
		"""
		Convert and save default tempo
		"""
		self.tempo =  int(round(60.0E06/float(parList[0]))) # convert to bpm
		
	def __doKey (self,parList):
		"""
		Save key
		"""
		self.key =  "%s %s" % (parList[0][0],parList[1][0])
		
	def __doTrackName (self,parList):
		"""
		Save track name (for multitrack)
		"""
		pass
	
	def __doSynth (self,parList):
		"""
		Choose the correct synth velocity conversion curve.
		"""
		pass
	
	def __doProgram (self,parList):
		"""
		Choose the midi program
		"""
		if not self.muted:
			print 'Hello'
			self.trackProgram = [int(parList[0]), int(parList[1])]
			self.player.programChange(int(parList[1]),int(parList[0])) # (Channel, program)
	
	def __doVol (self,parList):
		"""
		Read default midi volume and set it right
		"""
		if not self.muted:
			ch = int(parList[1])
			vol = self.__dB2vel(int(parList[0]),self.synth)
			self.trackVolume = [ch,vol]
			self.player.channelVolume(ch,vol)
	
	def __doTrackDelay (self,parList):
		"""
		Read track delay (?)
		"""
		pass
	
	def __doHeaderEnd (self,parList):
		"""
		Save header end position.
		"""
		self.trackStart = self.currentPosition + 1
		self.currentBar = [1, self.currentPosition]
		return 'headerend'
	
	def __doBar (self,parList):
		"""
		Read the number of the current bar
		"""
		self.currentBar = [int(parList[0]),self.currentPosition]
		return 'bar'
	
	def __doDT (self,parList):
		"""
		Read the delta time value for the next note.
		"""
		self.DT = self.__str2float(parList)
		return 'dtRule'
	
	def __doDSL(self,parList):
		"""
		Read the Delta Sound Level for the next note
		"""
		self.DSL = self.__str2float(parList)
		return 'dslRule'
	
	def __doDART(self,parList):
		"""
		Read the Delta Articulation for the next note
		"""
		self.DART = self.__str2float(parList)
		return 'dartRule'
	
	def	__doNote(self,parList):
		"""
		Read the note to be played, compute the performance values and send the appropriate message to the midiplayer. In case of recording, save k, overall and xtflag in the score.
		"""
		parList = self.__str2float(parList)	
		# Compute note duration from nominal + delta
		dur = parList[3] + self.__dartModify()	
		# Compute note SL in dB from nominal + delta
		# Convert SL in dB to MIDI velocity
		#print parList[2]
		#print self.__dslModify()
		vel = self.__dB2vel(parList[2]+self.__dslModify(),self.synth)
		note = int(parList[0])
		ch = int(parList[1])
		if not self.muted: # in case of fastforward, don't play the note
			# Before sending a note one message, check if that note is already playing. In that case, stop it and play it again.
			for off in self.offList[:]:
				if off[0] == note and off[1] == ch:
					self.player.noteOff(ch,note)
					self.offList.remove(off)
			self.player.noteOn(ch,note,vel,dur)
			self.offList.append([note,ch,self.player.getMidiTime()+dur])
			if self.recording: # save kvector and koverall
				self.scoreFile[self.currentPosition-3] = "0 XY "+self.__list2str(self.flagXY)
				self.scoreFile[self.currentPosition-2] = "0 KVEC "+self.__list2str(self.kVector)
				self.scoreFile[self.currentPosition-1] = "0 KOVERALL "+self.__list2str(self.overallPar)
		return 'note'
		
	def __doKVec(self,parList):	
		"""
		Read K vector from file
		"""
		self.kVector = self.__str2float(parList)
		return 'kVec'
		
	def __doKOverall(self,parList):
		"""
		Read overall vector from file
		"""
		self.overallPar = self.__str2float(parList)
		return 'kOverall'
		
	def __doXY(self,parList):
		"""
		Read xyflag from file
		"""
		self.flagXY = self.__str2float(parList)
		
	def __doSusPed(self,parList):
		"""
		Handle sustain pedal on/off
		"""
		#print parList
		if not self.muted:
			self.player.sustainPedal(int(parList[0]))

	def __doOneStrPed(self,parList):
		"""
		Handle sof pedal (one string pedal)
		"""
		if not self.muted:
			self.player.softPedal(int(parList[0]))
			print parList
		
	def __errhandler (self,parList): pass



############################################################################################
# Utility functions to convert various variables

	def __dB2vel(self,sldB,synthMap):
		"""
		Convert sound level values from dB to midi velocity using the curves from the various synths
		"""
		temp = 0
		for n in range(0,3):
			temp = temp + pow(sldB,n) * synthMap[n]
		return int(round(temp))

	def __str2float(self,listStr):
		"""
		Convert list of str to list of float
		"""
		listFloat = []
		for item in listStr:
			listFloat.append(float(item))
		return listFloat


	def __float2int(self,listFloat):
		"""
		Convert list of float to list of int
		"""	
		listInt = []
		for item in listFloat:
			listInt.append(int(item))
		return listInt
		
	def __list2str(self,listFloat):
		"""
		COnvert list of float to a string
		"""
		listStr = ""
		for item in listFloat:
			listStr = listStr+" "+str(item)
		return listStr.strip()
		
	def __dtModify(self):
		"""
		Function that applies rules to delta times
		"""
		rules = [1,2,3,4,5,6,7,8,9,11,12,13,14,15]
		temp = 0
		m = 0
		for n in rules:
			temp = temp + self.kVector[n-1]*self.DT[m]
			m +=1	
		return (1+temp)*self.overallPar[0]
	
	def __dslModify(self):
		"""
		Function that applies rules to Sound Levels
		"""	
		rules = [1,2,3,4,5,6,7,10,11,12,13]
		temp = 0
		m = 0
		for n in rules:
			temp = temp + self.kVector[n-1]*float(self.DSL[m])
			m += 1
		return temp+self.overallPar[1]
	
	def __dartModify(self):
		"""
		Function that applies rules to the duration of a note (articulation)	
		"""
		rules = [9,16,17,18,19]
		temp = 0
		m = 0
		for n in rules:
			temp = temp + self.kVector[n-1]*self.DART[m]
			m += 1	
		return round(temp)
		
	
	def av2kvec(self,coordinates):
		"""
		Convert AV coordinates to k and overall vectors.
		@coordinates: [x,y]
		"""
		k = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		for n in range(0,len(k)):
			k[n] = 0.25 * ((__HAPPY_CORNER_K__[n] - __TENDER_CORNER_K__[n] - __ANGRY_CORNER_K__[n] + __SAD_CORNER_K__[n]) * coordinates[0] + __HAPPY_CORNER_K__[n] + __TENDER_CORNER_K__[n] - __ANGRY_CORNER_K__[n] - __SAD_CORNER_K__[n]) * coordinates[1] + 0.25 * ((__HAPPY_CORNER_K__[n] - __TENDER_CORNER_K__[n] + __ANGRY_CORNER_K__[n] - __SAD_CORNER_K__[n]) * coordinates[0] + __HAPPY_CORNER_K__[n] + __TENDER_CORNER_K__[n] + __ANGRY_CORNER_K__[n] + __SAD_CORNER_K__[n])
		overall = [1,1]
		for n in range(0,2):
			overall[n] = 0.25 * ((__HAPPY_CORNER_OVERALL__[n] - __TENDER_CORNER_OVERALL__[n] - __ANGRY_CORNER_OVERALL__[n] + __SAD_CORNER_OVERALL__[n]) * coordinates[0] + __HAPPY_CORNER_OVERALL__[n] + __TENDER_CORNER_OVERALL__[n] - __ANGRY_CORNER_OVERALL__[n] - __SAD_CORNER_OVERALL__[n]) * coordinates[1] + 0.25 * ((__HAPPY_CORNER_OVERALL__[n] - __TENDER_CORNER_OVERALL__[n] + __ANGRY_CORNER_OVERALL__[n] - __SAD_CORNER_OVERALL__[n]) * coordinates[0] + __HAPPY_CORNER_OVERALL__[n] + __TENDER_CORNER_OVERALL__[n] + __ANGRY_CORNER_OVERALL__[n] + __SAD_CORNER_OVERALL__[n])
		return [k,overall]